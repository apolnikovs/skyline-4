# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.3.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-26 15:28                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.129');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `manufacturer` DROP FOREIGN KEY `user_TO_manufacturer`;

ALTER TABLE `network_manufacturer` DROP FOREIGN KEY `manufacturer_TO_network_manufacturer`;

ALTER TABLE `model` DROP FOREIGN KEY `manufacturer_TO_model`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `unit_pricing_structure` DROP FOREIGN KEY `manufacturer_TO_unit_pricing_structure`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `manufacturer_TO_town_allocation`;

ALTER TABLE `central_service_allocation` DROP FOREIGN KEY `manufacturer_TO_central_service_allocation`;

ALTER TABLE `postcode_allocation` DROP FOREIGN KEY `manufacturer_TO_postcode_allocation`;

ALTER TABLE `bought_out_guarantee` DROP FOREIGN KEY `manufacturer_TO_bought_out_guarantee`;

# ---------------------------------------------------------------------- #
# Modify table "manufacturer"                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `manufacturer` ADD COLUMN `AuthorisationRequired` ENUM('Yes','No');

ALTER TABLE `manufacturer` MODIFY `AuthorisationRequired` ENUM('Yes','No') AFTER `Status`;

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `manufacturer` ADD CONSTRAINT `user_TO_manufacturer` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `network_manufacturer` ADD CONSTRAINT `manufacturer_TO_network_manufacturer` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `model` ADD CONSTRAINT `manufacturer_TO_model` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `unit_pricing_structure` ADD CONSTRAINT `manufacturer_TO_unit_pricing_structure` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `manufacturer_TO_town_allocation` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `central_service_allocation` ADD CONSTRAINT `manufacturer_TO_central_service_allocation` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `postcode_allocation` ADD CONSTRAINT `manufacturer_TO_postcode_allocation` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `bought_out_guarantee` ADD CONSTRAINT `manufacturer_TO_bought_out_guarantee` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.130');
