# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-11-09 09:07                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.118');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `status_preference` DROP FOREIGN KEY `user_TO_status_preference`;

# ---------------------------------------------------------------------- #
# Modify table "status_preference"                                       #
# ---------------------------------------------------------------------- #

DROP INDEX `IDX_status_preference_1` ON `status_preference`;

CREATE UNIQUE INDEX `IDX_status_preference_1` ON `status_preference` (`Page`,`UserID`,`Type`,`StatusID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `status_preference` ADD CONSTRAINT `user_TO_status_preference` 
    FOREIGN KEY (`UserID`) REFERENCES `user` (`UserID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.119');
