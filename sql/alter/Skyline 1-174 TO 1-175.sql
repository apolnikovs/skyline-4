# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.174');

# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer_details"                                                  #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer_details`
ALTER `StartPostcode` DROP DEFAULT,
ALTER `EndPostcode` DROP DEFAULT;
ALTER TABLE `service_provider_engineer_details`
CHANGE COLUMN `StartPostcode` `StartPostcode` VARCHAR(100) NOT NULL AFTER `EndShift`,
CHANGE COLUMN `EndPostcode` `EndPostcode` VARCHAR(100) NOT NULL AFTER `StartPostcode`;

 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.175');
