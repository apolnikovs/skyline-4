# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- #

call UpgradeSchemaVersion('1.217');

# ---------------------------------------------------------------------- #
# Modify Table appointment                                               #
# ---------------------------------------------------------------------- #
ALTER TABLE appointment ADD COLUMN SamsungOneTouchChangeReason VARCHAR(100) NULL AFTER ForceAddReason;




CREATE TABLE diary_faq (
                        DiaryFaqID INT(10) NOT NULL AUTO_INCREMENT,
                        DiaryFaqCategoryID INT(10) NULL,
                        FaqQuestion VARCHAR(500) NULL,
                        FaqAnswer VARCHAR(5000) NULL,
                        Status ENUM('Active','In-Active') NOT NULL DEFAULT 'Active',
                        PRIMARY KEY (DiaryFaqID)
                        ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE diary_faq_category (
                                DiaryFaqCategoryID INT(10) NOT NULL AUTO_INCREMENT,
                                CategoryName VARCHAR(50) NULL DEFAULT NULL,
                                PRIMARY KEY (DiaryFaqCategoryID)
                                ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.218');