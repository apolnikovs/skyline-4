<script type="text/javascript">   
     
 $(document).ready(function() {
 
                                $('#ModelsID').multiselect( {  noneSelectedText: "{$page['Text']['select_models']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500]  } );

 
 $(function() {
    $( "#tabs" ).tabs({
    show: function(event, ui) {
        $.colorbox.resize();
    }
});
  });
    if($('#ForceJobFaultCode').attr('checked')=="checked"){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide() ;$.colorbox.resize();}
    if($('#RestrictLookup').attr('checked')=="checked"){ $('#addFieldsDop2').show();$.colorbox.resize();}else{ $('#addFieldsDop2').hide();$.colorbox.resize(); }
    
    //getting the models based on manufacturer
         $('#Manufacturer').on('change',function()
            {
                if($('#Manufacturer').val() != ''){
                
                //alert($('#Manufacturer').val());
                
                       $.ajax({
                        type:   "POST",
                        url:    "{$_subdomain}/Data/getModelsList/ManufacturerID="+$('#Manufacturer').val(),
                        data:   "",
                        dataType: "json",
                        async: false,
                        success:    function(data){
                            //var obj = jQuery.parseJSON( data );
                            //$('#ModelsID option').remove();  
                            
                                $('#ModelsIDHtml').html(''); 
                      
                                
                                $displayModelsList = "<select id='ModelsID' name='Models[]'  multiple='multiple' style='width:90px;' class='text auto-hint' >";
                               
                                $.each(data, function(i, item) {
                             
                                        $displayModelsList  += "<option value="+item.ModelID+">"+item.ModelNumber+"</option>"  ; 
                                        //alert(item.ModelNumber);
                                });

                                $displayModelsList  += " </select>"; 
                               // alert($test);
                                $('#ModelsIDHtml').html($displayModelsList); 
                               // alert($test);
                                 //Multi select check boxes
                                $('#ModelsID').multiselect( {  noneSelectedText: "{$page['Text']['select_models']|escape:'html'}", minWidth: 320, height: "auto", show:['slide', 500]  } );

                        },
                    });
             
                    
                }
            });
            
            
    // form validation
    
      $("#PartFaultCodeLookupForm").validate = null;
	
	$("#PartFaultCodeLookupForm").validate({
	    rules: {
                       
                        LookupName:{
                             required: function (element) {
                                    }    
                                  },
                        FaultCodeDescription:{
                             required: function (element) {
                                    }    
                                  },                                    
                        Manufacturer:{
                             required: function (element) {
                                   
                                     }
                                   },
                        FieldNumber:{
                             required: function (element) {
                                   
                                      }
                                   },
                           JobFaultCodeNo:
                                                    {
                                                        required: function (element) {
                                                            if($("#ForceJobFaultCode").is(':checked'))
                                                            {
                                                            return true;
                                                            }
                                                            else
                                                            {
                                                              return false;
                                                            }
                                                        },
                                                       digits: true
                                                    },
                           RestrictLookupTo:
                                                    {
                                                        required: function (element) {
                                                            if($("#RestrictLookup").is(':checked'))
                                                            {
                                                              return true;
                                                            }
                                                            else
                                                            {
                                                               return false;
                                                            }
                                                        },
                                                        //digits: true
                                                    } 
                    },
	   
 
	    errorPlacement: function(error, element) {
		error.appendTo(element.parent("p"));
                
                $.colorbox.resize();
                
	    },
	    ignore:	    "",
	    errorClass:	    "fieldError2",
	    onkeyup:	    false,
	    onblur:	    false,
	    errorElement:   "label"
	   
	});            
    
 $('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'click',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: false // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });  

}); 

  </script>  
    
    <div id="PartFaultCodeLookupFormPanel" class="SystemAdminFormPanel" >
    
                <form id="PartFaultCodeLookupForm" name="PartFaultCodeLookupForm" method="post"  action="{$_subdomain}/LookupTables/savePartFaultCodeLookup" class="inline" >
                    <input type="hidden" name="PartFaultCodeLookupID" value="{$datarow.PartFaultCodeLookupID|default:""}">
                <fieldset>
                    <legend title="" >Part Fault Code Lookups</legend>
                        
                 
                            
                        
                        
                            <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Lookup Details</a></li>
    <li><a href="#tabs-2">Linked Fault Codes</a></li>
  {*  <li><a href="#tabs-3">Action Settings</a></li>
    <li><a href="#tabs-4">Compulsory Settings</a></li>
    <li><a {if $datarow.PartFaultCodeID|default:''!=''}onclick="loadLinkedItem()" {/if} href="#tabs-5">Lookup Settings</a></li>
   *}
    
  </ul>
          <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

          </p>
          <div id="tabs-1" class="SystemAdminFormPanel inline">
              
          <p>
                                <label class="cardLabel" for="LookupName" >Lookup Name:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="LookupName" value="{$datarow.LookupName|default:''}" id="LookupName" >
          </p>
          
          <p>
                                <label class="cardLabel" for="FaultCodeDescription" >Fault Code Description:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input  type="text" class="text"  name="FaultCodeDescription" value="{$datarow.FaultCodeDescription|default:''}" id="FaultCodeDescription" >
          </p>
          
          <p>
                            <label class="cardLabel" for="Manufacturer" >Manufacturer:<sup>*</sup></label>
                            &nbsp;&nbsp; 
                                <select name="ManufacturerID" id="Manufacturer" class="text" >
                                <option value="" {if $datarow.ManufacturerID|default:"" eq ''}selected="selected"{/if}>Select from drop down</option>

                                
                                {foreach $manufacturerList|default:"" as $s}

                                    <option value="{$s.ManufacturerID|default:""}" {if $datarow.ManufacturerID|default:"" eq $s.ManufacturerID|default:""} selected="selected"{/if}>{$s.ManufacturerName|default:""}</option>

                                {/foreach}
                                
                            </select>
         </p>
         

          
           <p>
                                <label class="cardLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
                                    
				<input  type="checkbox" name="Status"  value="In-active" {if $datarow.Status|default:'' eq 'In-active'}checked="checked"{/if}  /> Inactive &nbsp;
					</span>

                                    
          </p>
          <p>
                                <label class="cardLabel" for="FieldNumber" >Field Number:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                <input style="width:142px"  type="text" class="text"  name="FieldNumber" value="{$datarow.FieldNumber|default:''}" id="FieldNumber" >
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCode_FieldNumber_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
          </p>
          
        <p>
                                <label class="cardLabel" for="ForceJobFaultCode" >Force Job Fault Code:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input onclick="if(this.checked==true){ $('#addFieldsDop').show();$.colorbox.resize();}else{ $('#addFieldsDop').hide();$.colorbox.resize(); }"  type="checkbox" name="ForceJobFaultCode" id="ForceJobFaultCode" value="Yes" {if $datarow.ForceJobFaultCode|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_ForceJobFaultCode_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
        </p>
          <div id="addFieldsDop" style="display:none">
        <p>
                                <label class="cardLabel" for="JobFaultCodeNo" >Job Fault Code No:<sup>*</sup></label>
                                &nbsp;&nbsp;
                                {* <input style="width:142px"  type="text" class="text"  name="JobFaultCodeNo" value="{$datarow.JobFaultCodeNo|default:''}" id="JobFaultCodeNo" maxlength="2"> *}
                               
                                <select  name="JobFaultCodeNo" id="JobFaultCodeNo" > 
                                    
                                {for $i=1 to 20}                                    
                                    <option value="{$i}" {if isset($datarow.JobFaultCodeNo) && $datarow.JobFaultCodeNo==$i} selected {/if} >{$i}</option>                               
                                 {/for}
                             </select> 
        </p>
        </div>
          
       <p>
                                <label class="cardLabel" for="RestrictLookup" >Restrict Lookup:</label>
                                &nbsp;
                                <span class="formRadioCheckText"  class="saFormSpan">
				<input onclick="if(this.checked==true){ $('#addFieldsDop2').show();$.colorbox.resize();}else{ $('#addFieldsDop2').hide();$.colorbox.resize(); }"  type="checkbox" name="RestrictLookup" id="RestrictLookup" value="Yes" {if $datarow.RestrictLookup|default:'' eq 'Yes'}checked="checked"{/if}  />
                                <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCodesLookup_RestrictLookup_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >
					</span>

                                    
        </p>
          <div id="addFieldsDop2" style="display:none">
        <p>
                                <label class="cardLabel" for="RestrictLookupTo" >Restrict Lookup To:<sup>*</sup></label>
                               &nbsp; <input {if $datarow.RestrictLookupTo|default:"Part Used"=="Part Used"}checked=checked{/if}  type="radio"  name="RestrictLookupTo" value="Part Used" id="RestrictLookupTo" >  Part Used
                                &nbsp; <input {if $datarow.RestrictLookupTo|default:"Part Used"=="Part Correction"}checked=checked{/if}  type="radio"  name="RestrictLookupTo" value="Part Correction" id="RestrictLookupTo" > Part Correction        
                                &nbsp; <input {if $datarow.RestrictLookupTo|default:"Part Used"=="Adjustment"}checked=checked{/if}  type="radio"  name="RestrictLookupTo" value="Adjustment" id="RestrictLookupTo" > Adjustment        
                             
                                
        </p>
        </div>      
                                
          <p>
                            <label class="cardLabel" for="JobTypeAvailability" >Job Type Availability:</label>
                            &nbsp; <input {if $datarow.JobTypeAvailability|default:"Both"=="Chargeable Jobs"}checked=checked{/if}  type="radio"  name="JobTypeAvailability" value="Chargeable Jobs" id="JobTypeAvailability" > Chargeable Jobs
                            &nbsp; <input {if $datarow.JobTypeAvailability|default:"Both"=="Warranty Jobs"}checked=checked{/if}  type="radio"  name="JobTypeAvailability" value="Warranty Jobs" id="JobTypeAvailability" > Warranty Jobs  
                            &nbsp; <input {if $datarow.JobTypeAvailability|default:"Both"=="Both"}checked=checked{/if}  type="radio"  name="JobTypeAvailability" value="Both" id="JobTypeAvailability" >   Both
                                   
                            
                             <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="partFaultCode_JobTypeAvailability_Help" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" >    
          </p>
          
          </div>
          
    <div id="tabs-2" class="SystemAdminFormPanel inline">
        
                 <p>
                     
                             <label class="fieldLabel">Models:</label> 
                            
                                <span id="ModelsIDHtml">
                                
                                    <select id='ModelsID' name='Models'  multiple='multiple' style='width:90px;' class='text auto-hint' >

                                {foreach $modelsList|default:"" as $s}
                                    
                                    <option value="{$s.ModelID|default:""}" {if $s.ModelsID|default:""}{/if}  selected="selected">{$s.ModelNumber|default:""}</option>
 selected="selected"
                                {/foreach}
                                        
                                    </select>
                                    
                             </span>
                             
                             
                             
                             
                               {* <span><a href="#" onclick="addModels(); return false;"><img src="{$_subdomain}/css/Skins/{$_theme}/images/add_icon.png" width="20" height="20"  > </a></span>*}
                                                  
         </p>
        
       {*  <p>
                            <label class="cardLabel" for="LinkedFaultCodeFunctionality" >Linked Fault Code Functionality:</label>
                            &nbsp;&nbsp; 
                                      <select  name="Models" id="LinkedFaultCodeFunctionality"  multiple="multiple" style="width:90px;" class="text auto-hint" >
                                                          
                                        {foreach from=$modelsList item=sset}
                                         <option value="{$sset.ModelID}" {if in_array($sset.ModelID, $datarow.JobTypeAvailability)}selected="selected"{/if} >{$sset.ModelName|escape:'html'}</option>
                                         {/foreach}

                                     </select>  
         </p>  *} 
      
    </div>
 
                               
     
  <hr>
                               
                                <div style="height:20px;margin-bottom: 10px;text-align: center;">
                                <button type="submit" style="margin-left:38px" class="gplus-blue centerBtn">Save</button>
                                <button type="button" onclick="$.colorbox.close();"  class="gplus-blue" style="float:right">Cancel</button>
                                </div>

                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
 
                          
                        
