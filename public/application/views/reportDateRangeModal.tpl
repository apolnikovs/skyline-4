<script>

$(document).ready(function() {


    $(document).on("click", "#cancel", function() {
	$.colorbox.close();
	return false;
    })


    $(document).on("click", "#run", function() {
	
	if ($("#rowCount").text() == "0") {
	    modAlert("You can't run a report with 0 rows.");
	    return false;
	}
	
	if ($("#dateFrom").val() != "" || $("#dateTo").val() != "" || $("input[name=column]:checked").length != 0) {
	    if (($("#dateFrom").val() != "" || $("#dateTo").val() != "") && $("input[name=column]:checked").length == 0) {
		modAlert("If you want to apply date range you need to select Date Type.");
		return false;
	    }
	    /*
	    if (($("#dateFrom").val == "" && $("#dateTo").val == "") && $("input[name=column]:checked").length == 1) {
		modAlert("If you have selected Date Type you must");
		return false;
	    }
	    */
	}
	
	document.body.style.cursor = "wait";
	var filter = { };
	filter.column = $("input[name=column]:checked").val();
	filter.dateFrom = $("#dateFrom").val();
	filter.dateTo = $("#dateTo").val();
	$.ajax({
	    type:   "POST",
	    url:    "{$_subdomain}/Report/runUserReport",
	    data: { 
		reportID: {$id},
		filter: filter
	    }
	}).done(function(response) {
	    var data = JSON.parse(response);
	    document.body.style.cursor = "default";
	    $.colorbox.close();
	    location.href = "{$_subdomain}/reports/" + data + ".xlsx";
	});
	return false;
    })
    
    
    $(document).on("change", "input[type=text], input[name=column]", function() {
	getRowsFound();
    });
    
    
    $(document).on("change", "input[name=dateRangeType]", function() {
	var now = new Date();
	var from = new Date();
	var to = new Date();
	if ($(this).val() == "monthToDate") {
	    //$("#dateFrom").val(from.getUTCFullYear() + "-" + ("0" + (now.getMonth() + 1)).slice(-2) + "-" + "01");
	    //$("#dateTo").val(now.getUTCFullYear() + "-" + ("0" + (now.getMonth() + 1)).slice(-2) + "-" + ("0" + now.getDate()).slice(-2));
	    $("#dateFrom").val("01/" + ("0" + (now.getMonth() + 1)).slice(-2) + "/" + from.getUTCFullYear());
	    $("#dateTo").val(("0" + now.getDate()).slice(-2) + "/" + ("0" + (now.getMonth() + 1)).slice(-2) + "/" + now.getUTCFullYear());
	}
	if ($(this).val() == "lastMonth") {
	    from.setMonth(now.getMonth() - 1);
	    to.setMonth(now.getMonth() - 1);
	    //$("#dateFrom").val(from.getUTCFullYear() + "-" + ("0" + (from.getMonth() + 1)).slice(-2) + "-01");
	    //$("#dateTo").val(to.getUTCFullYear() + "-" + ("0" + (to.getMonth() + 1)).slice(-2) + "-" + ("0" + new Date(to.getUTCFullYear(), (to.getUTCMonth() + 1), 0).getDate()).slice(-2));
	    $("#dateFrom").val("01/" + ("0" + (from.getMonth() + 1)).slice(-2) + "/" + from.getUTCFullYear());
	    $("#dateTo").val(("0" + new Date(to.getUTCFullYear(), (to.getUTCMonth() + 1), 0).getDate()).slice(-2) + "/" + ("0" + (to.getMonth() + 1)).slice(-2) + "/" + to.getUTCFullYear());
	}
	if ($(this).val() == "yearToDate") {
	    //$("#dateFrom").val(now.getUTCFullYear() + "-01-01");
	    //$("#dateTo").val(now.getUTCFullYear() + "-" + ("0" + (now.getMonth() + 1)).slice(-2) + "-" + ("0" + now.getDate()).slice(-2));
	    $("#dateFrom").val("01/01/" + now.getUTCFullYear());
	    $("#dateTo").val(("0" + now.getDate()).slice(-2) + "/" + ("0" + (now.getMonth() + 1)).slice(-2) + "/" + now.getUTCFullYear());
	}
	if ($(this).val() == "dateRange") {
	    $("#dateFrom").val("");
	    $("#dateTo").val("");
	}
	if($("input[name=column]:checked").length != 0) {
	    getRowsFound();
	}
    });


    function getRowsFound() {
    
	if (($("#dateFrom").val() != "" || $("#dateTo").val() != "") && $("input[name=column]:checked").length == 0) {
	    return false;
	}
    
	$("#rowCount").html("<img src='{$_subdomain}/images/spinner-circle.gif' style='margin-top:-3px;' />");
	
	var filter = { };
	filter.column = $("input[name=column]:checked").val();
	filter.dateFrom = $("#dateFrom").val();
	filter.dateTo = $("#dateTo").val();
    
	$.ajax({
	    type:   "POST",
	    url:    "{$_subdomain}/Report/getReportRowCount",
	    data: { 
		id: {$id},
		filter: JSON.stringify(filter)
	    }
	}).done(function(response) {
	    if(!response) {
		response = 0;
	    }
	    $("#rowCount").text(response);
	});
    
    }


});

</script>


<div id="reportDateRangeModal" style="display:block; position:relative; float:left; width:100%;">

    <div id="rowCountDiv" style="position:absolute; right:10px; top:8px; z-index:9999; background:#FFF; padding:0px 10px;">
	<span id="rowCount">{$rowCount}</span> Rows found
    </div>
    
    <fieldset style="width:100%; position:relative; display:block; float:left;">
	<legend>Date Range Criteria</legend>
	<fieldset id="columns" style="display:block; position:relative; float:left; width:100%; margin-top:10px;">
	    <legend>Date Type</legend>
	    {foreach $columns as $column}
		<div>
		    <input type="radio" name="column" value="{$column["colTable"]}.{$column["colName"]}" /> {$column["structure"]->Name}
		</div>
	    {/foreach}
	</fieldset>
	<fieldset id="dateRange" style="display:block; position:relative; float:left; width:100%; margin-top:10px;">
	    <legend>Date Range</legend>
	    <div id="dateRangeType" style="display:block; position:relative; float:left; width:50%;">
		<input type="radio" name="dateRangeType" value="monthToDate" /> Month to date <br/>
		<input type="radio" name="dateRangeType" value="lastMonth" /> Last month <br/>
		<input type="radio" name="dateRangeType" value="yearToDate" /> Year to date <br/>
		<input type="radio" name="dateRangeType" value="dateRange" /> Date range <br/>
	    </div>
	    <div id="dateRangeInput" style="display:block; position:relative; float:left; width:50%;">
		<label for="dateFrom" style="width:80px; font-size:12px;">Date from:</label>
		<input type="text" name="dateFrom" id="dateFrom" style="width:80px;" />
		<label for="dateTo" style="width:80px; font-size:12px;">Date to:</label>
		<input type="text" name="dateTo" id="dateTo" style="width:80px;" />
	    </div>
	</fieldset>
	
	<div style="position:relative; display:block; float:left; margin:10px 0px 0px 0px; text-align:center; width:100%;">
	    <a href="#" class="btnConfirm" id="run" style="width:60px; margin-right:10px;">Run</a>
	    <a href="#" class="btnCancel" id="cancel" style="width:60px;">Cancel</a>
	</div>
	
    </fieldset>

</div>