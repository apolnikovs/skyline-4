 <script type="text/javascript">   
     
 $(document).ready(function() {
 
 $('.viewA').click(function(event){

console.log($(this).closest('tr'))

});
 
  $('#stockHistoryDataTable').dataTable( {
"sDom": 'f<bottom>pi',
//"sDom": 'Rlfrtip',
"bServerSide":true,
"bPaginate":true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 ], [25, 50, 100]],

		 "oLanguage": {
                                
                                "sSearch": "Filter Results By:"
                            },
    "sAjaxSource": "{$_subdomain}/StockControl/loadStockHistoryList/id={$id}/",
     
                "fnServerData": function ( sSource, aoData, fnCallback ) {
			/* Add some extra data to the sender */
			aoData.push( { "name": "more_data", "value": "my_value" } );
			$.getJSON( sSource, aoData, function (json) { 
				/* Do whatever additional processing you want on the callback, then tell DataTables */
				fnCallback(json)
                               
                              
                                $.colorbox.resize();
                               
                               
			} );
                        },
                        
                            
                        

"iDisplayLength" : 25,

"aoColumns": [ 
    
			 { "bVisible":0, "bSearchable":0},
			 { "bVisible":1, "bSearchable":1},
			 { "bVisible":1, "bSearchable":1},
			 { "bVisible":1, "bSearchable":0},
			 { "bVisible":1, "bSearchable":1},
                             true,
                             true,
                             true,
                             true,
                             true,
                             true,
                             true,
                             true,
                          { "bVisible":1, "bSearchable":0},
                              
                            
		],
                "bFilter": true,
                 "aaSorting": [ [0,'desc'] ]
              
               
   
 
        
          
});//datatable end
   }); 
   
   function showNotes(e){
  var id=$(e).closest('tr').attr('id');
  $('#dialogDiv').load('{$_subdomain}/StockControl/showHistoryNote/'+id)
 .dialog(
{       
      title:"Notes",
      resizable:false,
      modal: true,
      buttons: {
        Ok:{  text:"Ok","class":"gplus-blue", click:function() {
          $( this ).dialog( "close" );
          $('#dialogDiv').html('');
        }}
      }
    });
    
   }
   
  </script>  
   
<div id="StockHistoryPopupContainer" class="SystemAdminFormPanel" >
    <fieldset>
        <legend>Stock Part History</legend>
         <label  style="width:120px;font-size:12px">Stock No:</label><span >{$Data.PartNumber|default:''}</span><br>
         <label  style="width:120px;font-size:12px">Description:</label><span >{$Data.Description|default:''}</span><br>
         <label  style="width:120px;font-size:12px">Current Qty:</label><span >{$Data.InStockAvailable|default:''}</span><br>
    
            <table id="stockHistoryDataTable" border="0" cellpadding="0" cellspacing="0" class="browse">
                <thead>
                    
                <tr>
                  
                    <th>Id</th>
                    <th>Incoming / Outgoing</th>
                    <th>Date</th>
                    <th>Order No</th>
                    <th>Job No</th>
                    <th>User</th>
                    <th>Supplier</th>
                    <th>Quantity</th>
                    <th>Despatch No</th>
                    <th>Invoice No</th>
                    <th>Cost</th>
                    <th>GRN No</th>
                    <th>Comment</th>
                    <th>Note</th>
                   
                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
          
            <br>
              <button  style="width:150px;float:left;margin-right:10px;" onclick="" id="" type="button" class="gplus-blue">Export Data</button>
              <button  style="width:150px;float:left;" onclick="" id="" type="button" class="gplus-blue">Location History</button>
        
    
    </fieldset>    
                 
                        
       
</div>
         
         <div id="dialogDiv" style="display:none">
        
         </div>
                 
 
                          
                        
