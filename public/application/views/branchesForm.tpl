{*

* ??/??/??     1.0    Nageswara Rao Kanteti   <nag.phpdeveloper@gmail.com>
* 07/09/2012   1.1    Nageswara Rao Kanteti   <nag.phpdeveloper@gmail.com>  Enhancements


*}

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" /> *}


{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    <script type="text/javascript" >
        
        function addFields(selected) {	
                    var selected_split = selected.split(","); 	

                        

                    $("#BuildingNameNumber").val(selected_split[0]).blur();	
                    $("#Street").val(selected_split[1]).blur();
                    $("#LocalArea").val(selected_split[2]).blur();
                    $("#TownCity").val(selected_split[3]).blur();
                    $("#CountyID").val(selected_split[4]).blur();
                    $("#CountyID").trigger("change");
                   // $("#CountryID").val(selected_split[5]).blur();
                    
                    $("#selectOutput").slideUp("slow"); 

                }
                
              
    function setCountryBasedOnCounty($countyId, $countryId, $parentCountryId)
    {
        $($countyId+" option:selected").each(function () {
            $selected_cc_id =  $($countyId+" option:selected").attr('id');
            $country_id_array = $selected_cc_id.split('_');
            if($country_id_array[1]!='0')
            {
                $($countryId).val($country_id_array[1]);
                setValue($countryId);
            }
            else
            {
                 $($parentCountryId+" input:first").val('');
            }
        });
    }    
    $(document).ready(function() {
        $("#NetworkID").combobox({
            change: function() {
                $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';          
                var $NetworkID = $("#NetworkID").val();
                if($NetworkID && $NetworkID!='')
                {
                    $networkClients = $clients[$NetworkID];
                    if($networkClients)
                    {
                        for(var $i=0;$i<$networkClients.length;$i++)
                        {
                            $clientDropDownList += '<option value="'+$networkClients[$i][0]+'" >'+$networkClients[$i][1]+'</option>';
                        }
                    }
                }
                $("#ClientID").html($clientDropDownList);
            }
        });
        var  $brandCheckBoxesList = '';
        var  $brandRadioButtonsList = ''
        $("#ClientID").combobox({
            change: function() {
                $brandRadioButtonsList = '';
                $brandCheckBoxesList = '';
                var $ClientID = $("#ClientID").val();
                if($ClientID && $ClientID!='')
                {
                    $clientBrands = $brands[$ClientID];
                    if($clientBrands)
                    {
                        for(var $i=0;$i<$clientBrands.length;$i++)
                        {
                           $brandRadioButtonsList   += '<input  type="radio" name="BrandID"  value="'+$clientBrands[$i][0]+'" title="'+$clientBrands[$i][1]+'" /><span class="text" >'+$clientBrands[$i][1]+'</span><br>';
                           $brandCheckBoxesList     += '<input class="bChkBox"  type="checkbox" name="BrandID[]"  value="'+$clientBrands[$i][0]+'" /><span class="text" >'+$clientBrands[$i][1]+'</span><br>';
                        }
                    }
                }
                if($("#BranchType").val()=="Call Centre")
                {
                     $("#BrandID").html($brandCheckBoxesList);
                }
                else
                {
                     $("#BrandID").html($brandRadioButtonsList);  
                }
            }
        });
        $("#BranchType").combobox({
            change: function() {
                $clientProperty =   $("#ClientID").attr("disabled");            
                if($clientProperty=="disabled") 
                {    
                    $("#ClientID").removeAttr("disabled");
                }
                $("#ClientID").trigger("change");
                if($clientProperty=="disabled") 
                {    
                    $("#ClientID").attr("disabled", "disabled");
                }
                if($("#BranchType").val()=="Call Centre")
                {
                     $("#BrandID").html($brandCheckBoxesList);
                }
                else
                {
                     $("#BrandID").html($brandRadioButtonsList);  
                }
            }
        });
        $("#CountyID").combobox({
            change: function() {
                setCountryBasedOnCounty("#CountyID", "#CountryID", "#P_CountryID");
            }
        });
        $("#CountryID").combobox();
        $("#SMSID").combobox({
	    create: function(event, ui) {
		$(event.target.nextSibling).find(".ui-combobox-input").css("width", "276px");
	    }
	});
        
    
	    $("select[name=DefaultServiceProvider]").combobox();
	    $("select[name=ThirdPartyServiceProvider]").combobox();
            $("select[name=CurrentLocationOfProduct]").combobox();
    
            $('.fieldLabel').css('width', '250px');
      
    
            {if $User->NetworkID}
                
                $("#NetworkID").attr("disabled", "disabled");
                
            {/if}    
                
            {if $User->ClientID}
                
                $("#ClientID").attr("disabled", "disabled");
                
             {/if}      
                 
               
             {*{if $cBrands|@count gt 0}
                 
                {if $datarow.BranchType eq "Call Centre"}

                    $('.bChkBox').trigger('click');

                {else}

                    $('input[name=BrandID]:radio:checked').trigger('click');

                {/if}
                  
             {/if}*}     
        
             function autoHint()
             {
                 $('.auto-hint').each(function() {
                                    $this = $(this);
                                    if ($this.val() == $this.attr('title')) {
                                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','password');
                                        }
                                    }
                        } );  
             }
             
             
             
             $(document).on('click', '#insert_save_btn', 
                                function() {
                                
                                autoHint();
                
               });
               
             $(document).on('click', '#update_save_btn', 
                                function() {
                                
                                autoHint();
                
               });   
                
                
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="BranchesFormPanel" class="SystemAdminFormPanel" >
    
                <form id="BranchesForm" name="BranchesForm" method="post" action="#" class="inline" autocomplete="off" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                        <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                        </p>
                       
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['service_network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                            
                            
                          
                          <p>
                            <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $nClients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                          <p>
                                <hr>
                          </p>
                          
                          
                           <p>
                                <label class="fieldLabel" for="BranchType" >{$page['Labels']['branch_type']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                                    <select name="BranchType" id="BranchType" class="text" >
                                    <option value="" {if $datarow.BranchType eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $branchTypes as $branchType}

                                        <option value="{$branchType.BranchTypeID}" {if $datarow.BranchType eq $branchType.BranchTypeID}selected="selected"{/if}>{$branchType.Name|escape:'html'}</option>

                                    {/foreach}

                                </select>
                            </p>
                            
                          
                          
                          <p>
                            <label class="fieldLabel" for="BrandID" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
                           
                            
                             <div id="BrandID" class="checkBoxText" >
                                 
                                 {if $datarow.BranchType eq "Call Centre"}
                                     {foreach $cBrands as $brand}
                                        
                                        <input  type="checkbox" class="bChkBox" name="BrandID[]"  value="{$brand.BrandID}" {if in_array($brand.BrandID, $datarow.BrandID)} checked="checked" {/if}  /><span class="text" >{$brand.BrandName|escape:'html'}</span><br> 
                                      {/foreach}
                                      
                                      
                                 {else}    
                                      {foreach $cBrands as $brand}
                                         <input  type="radio" name="BrandID"  value="{$brand.BrandID}" title="{$brand.BrandName|escape:'html'}"  {if in_array($brand.BrandID, $datarow.BrandID)} checked="checked" {/if} /><span class="text" >{$brand.BrandName|escape:'html'}</span><br>   
                                      {/foreach}
                                 {/if}    
                                
                             </div>
                          </p>
                          
                          
                          {if $datarow.BranchID eq '' || $datarow.BranchID eq '0'}
                           <p>
                            <label class="fieldLabel" for="BranchLocation" >{$page['Labels']['branch_location_store_name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="BranchLocation" value="" id="BranchLocation" >
                        
                            </p>
                          {/if}
                          
                            <p>
                                  <hr>
                            </p>
                           
                            
                            <p>
                               <label class="fieldLabel" for="BranchName" >{$page['Labels']['name']|escape:'html'}:<sup>*</sup></label>

                                &nbsp;&nbsp; <input  type="text" class="text"  name="BranchName" value="{$datarow.BranchName|escape:'html'}" id="BranchName" >

                            </p>
                            
                            
                         
                            <p>
                                <label class="fieldLabel" for="BranchNumber" >{$page['Labels']['branch_number']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input type="text" class="text" name="BranchNumber" value="{$datarow.BranchNumber|escape:'html'}" id="BranchNumber" >          
                            </p>
                            
                         
                            <p>
                                <label class="fieldLabel" for="AccountNo" >{$page['Labels']['account_number']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input type="text" class="text" name="AccountNo" value="{$datarow.AccountNo|escape:'html'}" id="AccountNo" maxlength="40" >          
                            </p> 
                            
                            
                            
                            <p>
                                <hr>
                            </p>
                            
                         
                            <p>
                                <label class="fieldLabel" for="PostalCode">{$page['Labels']['postcode']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input  class="text uppercaseText" style="width: 140px;" type="text" name="PostalCode" id="PostalCode" value="{$datarow.PostalCode|escape:'html'}" >&nbsp;

                                <input type="submit" name="quick_find_btn" id="quick_find_btn" class="textSubmitButton postCodeLookUpBtn"  onclick="return false;"   value="Click to find Address" >

                            </p>

                            <p id="selectOutput" ></p>


                            <p>
                                <label class="fieldLabel" for="BuildingNameNumber" >{$page['Labels']['building_name']|escape:'html'}:</label>
                                &nbsp;&nbsp; <input type="text" class="text"  name="BuildingNameNumber" value="{$datarow.BuildingNameNumber|escape:'html'}" id="BuildingNameNumber" >          
                            </p>

                            <p>
                                <label class="fieldLabel" for="Street" >{$page['Labels']['street']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input type="text" class="text"  name="Street" value="{$datarow.Street|escape:'html'}" id="Street" >          
                            </p>

                            <p>
                                <label class="fieldLabel" for="LocalArea" >{$page['Labels']['area']|escape:'html'}:</label>
                                &nbsp;&nbsp; <input type="text" class="text" name="LocalArea" value="{$datarow.LocalArea|escape:'html'}" id="LocalArea" >          
                            </p>


                            <p>
                                <label class="fieldLabel" for="TownCity" >{$page['Labels']['city']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input type="text" class="text" name="TownCity" value="{$datarow.TownCity|escape:'html'}" id="TownCity" >          
                            </p>

                            <p>
                                <label class="fieldLabel" for="CountyID" >{$page['Labels']['county']|escape:'html'}:</label>
                                &nbsp;&nbsp; 


                                <select name="CountyID" id="CountyID" class="text" >
                                    <option value="" id="country_0_county_0" {if $datarow.CountyID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $countries as $country}

                                   
                                        {foreach $country.Counties as $county}
                                        <option value="{$county.CountyID}"  id="country_{$country.CountryID}_county_{$county.CountyID}" {if $datarow.CountyID eq $county.CountyID}selected="selected"{/if}>{$county.Name|escape:'html'}</option>
                                        {/foreach}
                                   

                                    {/foreach}

                                </select>

                            </p>



                            <p id="P_CountryID">
                                <label class="fieldLabel" for="CountryID" >{$page['Labels']['country']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                                    <select name="CountryID" id="CountryID" class="text" >
                                    <option value="" {if $datarow.CountryID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                    {foreach $countries as $country}

                                        <option value="{$country.CountryID}" {if $datarow.CountryID eq $country.CountryID}selected="selected"{/if}>{$country.Name|escape:'html'}</option>

                                    {/foreach}

                                </select>
                            </p>


                            <p>
				<label class="fieldLabel" for="ServiceManager">
				    {$page['Labels']['service_manager']|escape:'html'}:
				</label>
				&nbsp;&nbsp; 
				<input  type="text" name="ServiceManager" class="text auto-hint" title="{$page['Labels']['service_manager']|escape:'html'}" value="{$datarow.ServiceManager|escape:'html'}" id="ServiceManager" />
				<br>
                            </p>
                            
			    <p>
				<label class="fieldLabel" for="ContactEmail">{$page['Labels']['email']|escape:'html'}:<sup>*</sup></label>
				&nbsp;&nbsp; <input  type="text" name="ContactEmail" class="text auto-hint" title="{$page['Text']['email']|escape:'html'}" value="{$datarow.ContactEmail|escape:'html'}" id="ContactEmail" >
				<br>
                            </p>

                            <p>
                                <label class="fieldLabel" for="ContactPhone" >{$page['Labels']['phone']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; <input  type="text" class="text phoneField"  name="ContactPhone" value="{$datarow.ContactPhone|escape:'html'}" id="ContactPhone" >
                                <input  type="text" class="text extField auto-hint" title="{$page['Text']['extension_no']|escape:'html'}" name="ContactPhoneExt" value="{$datarow.ContactPhoneExt|escape:'html'}" id="ContactPhoneExt" >

                            </p>

                            <p>
				<label class="fieldLabel" for="ContactFax">{$page['Labels']['contact_fax']|escape:'html'}: </label>
				&nbsp;&nbsp; <input  type="text" name="ContactFax" maxlength="40" class="text"  value="{$datarow.ContactFax|escape:'html'}" id="ContactFax" >
				<br>
                            </p>

                         
			    <p style="margin-bottom:5px;">
				<label class="fieldLabel" for="ServiceAppraisalRequired" >{$page['Labels']['service_appraisal_required']|escape:'html'}:<sup>*</sup></label>
				&nbsp;&nbsp; 
				<input type="radio" name="ServiceAppraisalRequired" value="Yes" {if isset($datarow.ServiceAppraisalRequired) && $datarow.ServiceAppraisalRequired == "Yes"}checked="checked"{/if} />
				<span class="text">Yes</span> 
				<input type="radio" name="ServiceAppraisalRequired" value="No" {if isset($datarow.ServiceAppraisalRequired) && $datarow.ServiceAppraisalRequired == "No"}checked="checked"{/if} />
				<span class="text">No</span> 
			    </p>
			    
			    
			    <p>
				<label class="fieldLabel" for="DefaultServiceProvider" >{$page['Labels']['default_service_provider']|escape:'html'}:</label>
				&nbsp;&nbsp; 
				<select name="DefaultServiceProvider">
				    <option value=""></option>
				    {foreach $serviceProviders as $provider}
					<option value="{$provider.ServiceProviderID}" {if isset($datarow.DefaultServiceProvider) && $provider.ServiceProviderID == $datarow.DefaultServiceProvider}selected="selected"{/if}>{$provider.CompanyName}</option>
				    {/foreach}
				</select>
			    </p>
			    
			    <p>
				<label class="fieldLabel" for="ThirdPartyServiceProvider" >{$page['Labels']['third_party_service_provider']|escape:'html'}:</label>
				&nbsp;&nbsp; 
				<select name="ThirdPartyServiceProvider">
				    <option value=""></option>
				    {foreach $serviceProviders as $provider}
					<option value="{$provider.ServiceProviderID}" {if isset($datarow.ThirdPartyServiceProvider) && $provider.ServiceProviderID == $datarow.ThirdPartyServiceProvider}selected="selected"{/if}>{$provider.CompanyName}</option>
				    {/foreach}
				</select>
			    </p>
                            
                            
                            <p>
				<label class="fieldLabel" for="CurrentLocationOfProduct" >{$page['Labels']['current_location_of_product']|escape:'html'}:</label>
				&nbsp;&nbsp; 
				<select name="CurrentLocationOfProduct">
				    <option value=""></option>
				    {foreach $product_location_rows as $product_location}
					<option value="{$product_location.ProductLocation}" {if $product_location.ProductLocation == $datarow.CurrentLocationOfProduct}selected="selected"{/if}>{$product_location.Name}</option>
				    {/foreach}
				</select>
			    </p>
                            
                            
                            <p>
                              <label class="fieldLabel" for="SMSID" >{$page['Labels']['send_repair_complete_text_message']|escape:'html'}:</label>

                               &nbsp;&nbsp;  

                               <input  type="checkbox" name="SendRepairCompleteTextMessage"  value="Yes" {if $datarow.SendRepairCompleteTextMessage eq "Yes"} checked="checked" {/if}  />

                               <select name="SMSID" id="SMSID" class="text" style="width:286px;" >
                                  <option value="" {if $datarow.SMSID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>
                                  {foreach $smsMessages as $sms}

                                    <option value="{$sms.SMSID}" {if $datarow.SMSID eq $sms.SMSID}selected="selected"{/if}>{$sms.SMSName|escape:'html'}</option>

                                   {/foreach}
                               </select>
                            </p>

			    
                            <p>
				<label class="fieldLabel" for="OriginalRetailerFormElement">{$page['Labels']['original_retailer']|escape:'html'}:<sup>*</sup></label>
				&nbsp;&nbsp; 
				<input type="radio" name="OriginalRetailerFormElement" value="FreeText" {if $datarow.OriginalRetailerFormElement == "FreeText"}checked="checked"{/if} /><span class="text">{$page['Text']['free_text']|escape:'html'}</span> 
                                <input type="radio" name="OriginalRetailerFormElement" value="Dropdown" {if $datarow.OriginalRetailerFormElement == "Dropdown"}checked="checked"{/if} /><span class="text">{$page['Text']['dropdown']|escape:'html'}</span> 
			    </p>
                            
                            <p>
				<label class="fieldLabel" for="OpenJobsManagement">Branch Open Job Management:<sup>*</sup></label>
				&nbsp;&nbsp; 
				<input type="radio" name="OpenJobsManagement" value="Yes" {if $datarow.OpenJobsManagement == "Yes"}checked="checked"{/if} /><span class="text">Yes</span> 
				<input type="radio" name="OpenJobsManagement" value="No" {if $datarow.OpenJobsManagement == "No"}checked="checked"{/if} /><span class="text">No</span> 
			    </p>
                            
			{if $datarow.BranchID neq '' && $datarow.BranchID neq '0'}
			     
			    <p>
				<hr>
			    </p>
			    
			    <p>
				<label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
				&nbsp;&nbsp; 
				{foreach $statuses as $status}
				    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 
				{/foreach}    
			    </p>
                             
			{else}
                             
                             <input  type="hidden" name="Status"  value="Active"   />
                          
                          {/if}
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="BranchID"  value="{$datarow.BranchID|escape:'html'}" >
                                   
                                 
                                    {if $datarow.BranchID neq '' && $datarow.BranchID neq '0'}
                                        
                                         <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
