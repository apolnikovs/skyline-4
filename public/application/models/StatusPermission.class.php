<?php

/**
 * StatusPermission.class.php
 * 
 * Routines for interaction with the status_permission table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.0
 * 
 * Changes
 * Date        Version Author                Reason
 * 27/03/2013  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class StatusPermission extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::StatusPermission();
    }
    
    /**
     * create
     *  
     * Create an Status Permission Type
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new status permission
     * 
     * @return array    (status - Status Code, message - Status message, id - statusPermissionId of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array(                                                   /* Appointment Type successfully created */
                             'status' => 'SUCCESS',
                             'statusPermissionId' => $this->conn->lastInsertId() /* Return the newly created appointment type's ID */
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'statusPermissionId' => 0,                           /* Not created no ID to return */
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
    
    /**
     * update
     *  
     * Update an permission type
     * 
     * @param array $args   Associative array of field values for to update the
     *                      status permission. The array must include the primary
     *                      key StatusPermissionID
     * 
     * @return array (status - Status Code, message - Status message, id - statusPermissionId of item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {

    }
    
    /**
     * delete
     *  
     * Delete an entry from the status permssion table. We can delete by  
     * StatusPermssionID  To select the record to delete we must pass
     * an array with record with the key being either StatusPermissionID
     * and the value being the appropriate ID to delete.
     * 
     * @param array $args ( StatusPermissionID => Value )
     * 
     * @return (status - Status Code, message - Status message)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function delete($arg) {
        $index  = array_keys($arg);
        
        $cmd = $this->table->deleteCommand( $index[0].' = '.$arg[$index[0]] );
        
        if ($this->Execute($this->conn, $cmd, $arg)) {
            $result =  array(                                                   /* Job successfully created */
                             'status' => 'SUCCESS',
                             'message' => 'Deleted'
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()                  /* Return the error */
                           );
        }
        return $result;
    }
            
    
    /**
     * fetch
     *  
     * Gets a list of brands/branches/clients/roles/users as requested by the
     * StatusPermissions Lookup screen. Really fetch is not a good name for this 
     * method as it does not actually fetch anything to do with the status_permissions
     * table, but it has to be names this way so as tof it in within the existing
     * way the lookup tables admin system works!
     * 
     * @param array $args - firstArg - The permission type to look at.
     * 
     * @return array    Matching records (or null if none found)
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * Queries modified to get total permission count. by srinivas
     **************************************************************************/
    public function fetch($args) {
        switch ($args['firstArg']) {
            case 'brand':
                $output = $this->ServeDataTables(
                        $this->conn, 
                        'brand b LEFT JOIN client c ON b.ClientID = c.ClientID',
                        array('BrandID', 'BrandName', 'ClientName',
                            " (
                                        SELECT count( EntityID )
                                        FROM status_permission
                                        WHERE BrandID = EntityID
                                        AND PermissionType = 'brand'
                                        ) AS total"), 
                        $args
                    );
                break;  
            
            case 'branch':
                $output = $this->ServeDataTables(
                        $this->conn, 
                        'branch',
                        array('BranchID', 'BranchName', 'BranchType',
                            " (
                                        SELECT count( EntityID )
                                        FROM status_permission
                                        WHERE BranchID = EntityID
                                        AND PermissionType = 'branch'
                                        ) AS total"), 
                        $args
                    );
                break;  
            
            case 'client':
                $output = $this->ServeDataTables(
                        $this->conn, 
                        'client c LEFT JOIN service_provider sp ON c.ServiceProviderID = sp.ServiceProviderID',
                        array('ClientID', 'ClientName', 'CompanyName',
                            " (
                                        SELECT count( EntityID )
                                        FROM status_permission
                                        WHERE ClientID = EntityID
                                        AND PermissionType = 'client'
                                        ) AS total"), 
                        $args
                    );
                break;
            
            case 'role':
                $output = $this->ServeDataTables(
                        $this->conn, 
                        'role',
                        array('RoleID', 'Name', 'UserType',
                            " (
                                        SELECT count( EntityID )
                                        FROM status_permission
                                        WHERE RoleID = EntityID
                                        AND PermissionType = 'role'
                                        ) AS total"
                            ), 
                        $args
                    );
                break;
                
            default:    /* user or anything else */
                $output = $this->ServeDataTables(
                        $this->conn, 
                        'user',
                        array(
                                'UserID', 'Username', 
                                'CONCAT(`ContactFirstName`," ",`ContactLastName`) AS `Description`',
                                " (
                                        SELECT count( EntityID )
                                        FROM status_permission
                                        WHERE UserID = EntityID
                                        AND PermissionType = 'user'
                                        ) AS total"
                            
                            ), 
                        $args
                    );
                break;
        }
        //var_dump($_POST);
        // var_dump($output);
        return $output;
        
     }
    
     
    /**
     * fetch
     *  
     * Gets a list of brands/branches/clients/roles/users as requested by the
     * StatusPermissions Lookup screen.
     * 
     * @param string $entityType - The type of entity we want to look up
     * @param integer $entityId - The id of the wntity we are interested in
     * 
     * @return array    Matching records (or null if none found)
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function fetchByEntityId($entityType, $entityId ) {
        switch ($entityType) {
            case 'brand':
                $sql = "
                        SELECT
                                `BrandID` AS `ID`, 
                                `BrandName` AS `Name`
                        FROM
                                `brand`
                        WHERE
                                `BrandID` = $entityId
                       ";
                
                $result = $this->Query($this->conn, $sql);
                break;  
            
            case 'branch':
                $sql = "
                        SELECT
                                `BranchID` AS `ID`, 
                                `BranchName` AS `Name`
                        FROM
                                `branch`
                        WHERE
                                `BranchID` = $entityId
                       ";
                
                $result = $this->Query($this->conn, $sql);
                break;  
            
            case 'client':
                $sql = "
                        SELECT
                                `ClientID` AS `ID`, 
                                `ClientName` AS `Name`
                        FROM
                                `client`
                        WHERE
                                `ClientID` = $entityId
                       ";
                
                $result = $this->Query($this->conn, $sql);
                break;
            
            case 'role':
                $sql = "
                        SELECT
                                `RoleID` AS `ID`, 
                                `Name`
                        FROM
                                `role`
                        WHERE
                                `RoleID` = $entityId
                       ";
                
                $result = $this->Query($this->conn, $sql);
                break;
                
            default:                                                            /* user or anything else */
                $sql = "
                        SELECT
                                `UserID` AS `ID`, 
                                CONCAT(`ContactFirstName`,' ',`ContactLastName`) AS `Name`
                        FROM
                                `user`
                        WHERE
                                `UserID` = $entityId
                       ";
                
                $result = $this->Query($this->conn, $sql);
                break;
        } /* switch */
        
        if ( count($result > 0) ) {
            return($result[0]);                                                 /* Match found so return it */
        } else {
            return(                                                             /* No match so return array of nulls */
                    array(
                        'ID' => null,
                        'Name' => null
                    )
            );
        } /* fi count $result > 0 */
    }  
    
    /**
     * processData
     * 
     * Deal with the data passed by the System Status Permissions form
     * 
     * @param array $args   Passed Arguments
     * 
     * @return 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
     public function processData($args) {
        if ($this->debug) $this->log($args);
         
        $add_permission = array();

        foreach($args as $key => $a){
            
            //PermissionID_x
            if(preg_match('/HiddenPermissionID_/', $key)){
                list($f, $PermissionID) = explode('_', $key);
                #echo $PermissionID;
                
                if($args['HiddenPermissionID_'.$PermissionID]=="Active")
                {        
                    $add_permission[] = array(
                        'EntityID' => $args['ID'], 
                        'PermissionType' => $args['entitytype'],
                        'StatusID' => $PermissionID
                        );
                }

            }
        }
       
        /*
         * remove all assigned_permission for this role
         */
        $where = "EntityID={$args['ID']} AND PermissionType='{$args['entitytype']}'";
        $delete_cmd = $this->table->deleteCommand( $where );
        if ($this->Execute($this->conn, $delete_cmd)) {
            /*
             * add assigned_permission(s)
             */

            foreach($add_permission as $row){
                $insert_cmd = $this->table->insertCommand( $row );
                if (!$this->Execute($this->conn, $insert_cmd, $row)) {
                    $this->controller->log('Error Adding Assigned Permissions Record: '.$insert_cmd);
                    $this->controller->log(var_export($row, true));
                    $this->controller->log($this->lastPDOError());                      
                }
            } 
        } else {
            $this->controller->log('Error Deleting Assigned Permissions Record: '.$delete_cmd);
            $this->controller->log($this->lastPDOError());
        }
        
        return array('status' => 'OK', 'message' => $this->controller->page['data_updated_msg']);
     }
    
     
    /**
     * fetchByEntity
     *  
     * Returns the matching records for a given permission type and Entity ID
     * The neabled colum will show whether that particular record is atgegd or not.
     * 
     * @param string $pt    Permission Type
     * 
     * @return array    Matching records (or null if none found)
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function fetchByEntity($pt, $id) {
        
        $sql = "
                SELECT
			s.`StatusID`,
			s.`StatusName`,
			IF(ISNULL(sp.`StatusPermissionID`),
				0
			,
				-1
			) AS `Enabled`
		FROM
			`status` s LEFT JOIN `status_permission` sp ON s.`StatusID` = sp.`StatusID`
		WHERE
			(
				sp.`PermissionType` = '$pt'
				AND sp.`EntityID` = $id
			)
                        OR ISNULL(sp.`StatusPermissionID`)
               ";
        
        $result = $this->Query($this->conn, $sql, null, PDO::FETCH_NUM);
        
        if (count($result) == 0) {
            return(null);
        } else {
            $output['aaData'] = $result;
            return ($output);
        }
    }
    
    /**
     * fetchByEntity
     *  
     * Returns the matching records for a given permission type and Entity ID.
     * This method only returns those "tagged" records that are active for the
     * entity and entity type.
     * 
     * @param string $pt    Permission Type
     * 
     * @return array    Matching records (or null if none found)
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function fetchByEntityTagged($pt, $id) {
        
        $sql = "
                SELECT
			s.`StatusID`,
			s.`StatusName`,
			IF(ISNULL(sp.`StatusPermissionID`),
				0
			,
				-1
			) AS `Enabled`
		FROM
			`status` s LEFT JOIN `status_permission` sp ON s.`StatusID` = sp.`StatusID`
		WHERE
			sp.`PermissionType` = '$pt'
			AND sp.`EntityID` = $id
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if (count($result) == 0) {
            return(null);
        } else {
            return ($result);
        }
    }

}
?>
