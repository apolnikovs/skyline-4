<?php
/**
 * UsersAPIController.class.php
 * 
 * Implementation of Users API for SkyLine
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.0
 */

require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH.'/controllers/API/SkylineAPI.class.php');

class UsersAPI extends SkylineAPI {
        
   
   
    
    /**
     * Description
     * 
     * Deal with get  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     */
    
    public function get( $args ) {
        /* If we have gotr here then we are authorised ! */
        
        $response = array (
                            'ResponseCode' => "SC0001",
                            'Response' => "Successful validation"
                          );
        
        $this->sendResponse(200, $response);

    }
        
}

?>