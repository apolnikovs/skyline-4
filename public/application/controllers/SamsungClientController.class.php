<?php

/**
 * SamsungClientController.class.php
 * 
 * Implementation of Client for Samsung API
 * Specification Section 8.5
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2013 PC Control Systems
 * @link       
 * @version    1.10
 * 
 * Changes
 * Date        Version Author                Reason
 * 03/08/2012  1.00    Andrew J. Williams    Initial Version
 * 06/11/2012  1.01    Andrew J. Williams    Issue 119 - Changes required to Samsung API - getServiceRequest
 * 07/11/2012  1.02    Andrew J. Williams    Issue 121 - Samsung REST Client appears to be timing out.
 * 07/11/2012  1.03    Andrew J. Williams    Issue 123 - Error calling putRepairStatusAction in Samsung API when no parts
 * 08/11/2012  1.04    Andrew J. Williams    Issue 125 - Samsung API changes
 * 14/11/2012  1.05    Andrew J. Williams    Issue 129 - putRepairStatus - Samsung upload issues
 * 15/11/2012  1.06    Andrew J. Williams    Issue 130 - Undefined variable: job_fields in Samsung Controller
 * 15/11/2012  1.07    Andrew J. Williams    Issue 131 - PutServiceRequest in Samsung API Should Always use Samsung as Network
 * 28/11/2012  1.08    Andrew J. Williams    Issue 143 - Samsung Skyline job upload issues
 * 29/04/2013  1.09    Andrew J. Williams    Issue 323 - Samsung Interface Should Check NonSkyline Jobs for Match
 * 23/05/2013  1.10    Andris Polnikovs      putStockPartOrderReq creating order on samsung gspn
 ******************************************************************************/

require_once('CustomSmartyController.class.php');

class SamsungClientController extends CustomSmartyController  {
    public $debug = false;                                                      /* Turn on or off debugging */
    public $config;  
    public $session;
    public $messages;
     
    public function __construct() {
        parent::__construct(); 

        $this->config = $this->readConfig('application.ini');                   /* Read Application Config file. */
        $this->session = $this->loadModel('Session');                           /* Initialise Session Model */
        $this->messages = $this->loadModel('Messages');                         /* Initialise Messages Model */
        $user_model = $this->loadModel('Users');
        $this->user = $user_model->GetUser( $this->session->UserID,true);
    }
    
    /**
     * getServiceRequestAction
     * 
     * Loop through each ServiceCentre and then call getServiceRequest for each
     * centre.
     * 
     * @param $args
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function getServiceRequestAction($args) {
        $network_service_provider_model = $this->loadModel("NetworkServiceProvider");
        
        $nsp = $network_service_provider_model->fetchWhere( '`SamsungDownloadEnabled` = TRUE' );
                
        foreach ($nsp as $provider) {
            $params ['COMPANY'] = $provider['CompanyCode'];
            $params ['JOB_TYPE'] = 'SRA';
            $params ['REQUESTER'] = $provider['AccountNo'];
            $params ['TR_NO'] = '';

            $this->getServiceRequestCompany(
                                            $params, 
                                            $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['SamsungApiPath'].$this->config['Samsung']['getServiceRequest'],
                                            $provider['TrackingUsername'], 
                                            $provider['TrackingPassword']
                                           );
        
        } /* next nsp */ 
    }
    
    /**
     * getServiceRequestCompany
     * 
     * Download the latest jobs to SkyLine from the Samsung API for a specific
     * company
     * 
     * @param $args Associatifve array of passed arguments
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function getServiceRequestCompany($args, $site, $user, $pass) {      
        $response = $this->samsungCall($args, $site, $user, $pass); 
        $this->log("getServiceRequest : xml Response:\n{$response['raw']}",'samsung_api_');
        
        if ($response['raw'] == false) {
            $this->log("xml Response empty aborting}",'samsung_api_');
            $this->log(var_export($response,true));
            return;
        }
        
        $fields = json_decode(json_encode($response['response']),TRUE);
        //$fields = json_decode(json_encode($response['response']['ASREQUEST_CIC_ASC']['AS_REQUEST']),TRUE );
               
        $fields = $this->clearArray($fields);
        
        if (isset($fields['AS_REQUEST'][0])) {
            if ($fields['AS_REQUEST'][0]['ASREQUEST_INFO']['COMPANY'] == "ZZZZ") {
                $this->log('No Jobs','samsung_api_'); 
                return;
            } 
            
            foreach ($fields['AS_REQUEST'] as $samsung_job ) {
                if ($samsung_job['ASREQUEST_INFO']['TRACKING']['TR_STATUS'] == 'ST051') {
                    $this->getServiceRequestCancelItem($samsung_job, $user, $pass);     /* Job Cancelled */
                } else {
                    $this->getServiceRequestItem($samsung_job, $user, $pass);   /* New job - create */
                }
            } /* next samsung_job */
        } else {
            if ($fields['AS_REQUEST']['ASREQUEST_INFO']['COMPANY'] == "ZZZZ") {
                $this->log('No Jobs','samsung_api_'); 
                return;
            }
            
            $this->getServiceRequestItem($fields['AS_REQUEST'], $user, $pass);
        } /* fi isset $fields['AS_REQUEST'][0] */
        
    }
    
    /**
     * getServiceRequestCancelItem
     * 
     * Deal with the xml related to aspecific  job, cancel it and send 
     * response back to Samsung
     * 
     * @param $samsungJob   The array converted from the xml relating to a specific job
     *        $user         Samsung User Name
     *        $pass         Samsung Password
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
     public function getServiceRequestCancelItem($samsung_job, $user, $pass) {
        $api_jobs = $this->loadModel('APIJobs');
        $contact_history_model = $this->loadModel('ContactHistory');
        $job_model = $this->loadModel('Job') ;
        $service_providers_model = $this->loadModel('ServiceProviders');
        $status_history_model = $this->loadModel('StatusHistory');
        $skyline_model = $this->loadModel('StatusHistory');
        
        $jobId = $job_model->getIdFromNetworkRef($nRef);
        if (is_null($jobId)) {
            $msg = array(
                        'Code' => 'ERROR',
                        'Response' => "getServiceRequestCancelItem : Network Ref (TR_NO) {$samsung_job['ASREQUEST_INFO']['TR_NO']} no such job exists in Skyline."
                        );
            $this->log($msg['Response'],'samsung_api_');
            return($msg);
        }
        $spId = $job_model_model->getServiceCentre($jobId);
         
        $sUpdate = array (
                          'JobID' => $jobId,
                          'StatusID' => $skyline_model->getStatusID('29 JOB CANCELLED'),        
                          'UserID' => 4,
                          'Date' => date('Y-m-d H:i:s'),
                          'AdditionalInformation' => 'Cancelled by Samsung'
                         );
        
        $status_history_model->create($sUpdate);
        
        $ch = array(
                    'JobID' => $jobId,
                    'ContactHistoryActionID' => 14,
                    'ContactDate' => date('Y-m-d'),
                    'ContactTime' => date('H:i:s'),
                    'Note' => 'Cancelled by Samsung'
                   );
        
        $chResult = $contact_history_model->create($ch);
        
        /*
         * Response to Samsung to comn
         */

        $putServiceResponse = array();                                          /* Array to create response */

        $putServiceResponse['COMPANY'] = $network_service_provider_array['CompanyCode'];
        $putServiceResponse['JOB_TYPE'] = 'PCA';
        $putServiceResponse['REQUESTER'] = $samsung_job['ASREQUEST_INFO']['ASC_CODE'];
        $putServiceResponse['TR_NO'] = $samsung_job['ASREQUEST_INFO']['TR_NO'];
        $putServiceResponse['JOB_NO'] = $jobId;

        $site = $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['SamsungApiPath'].$this->config['Samsung']['putServiceResponse'];

        $psresponse = $this->samsungCall($putServiceResponse, $site, $user, $pass); 

        $this->log("putServiceResponse : xml Response:\n{$psresponse['raw']}",'samsung_api_');
        
        if ($psresponse['raw'] == false) {
            $this->log("xml Response empty aborting",'samsung_api_');
            $this->log(var_export($psresponse,true));
            return;
        }

        $fieldsPutServiceResponse = json_decode(json_encode($psresponse['response']),TRUE);

        $fieldsPutServiceResponse = $this->clearArray($fieldsPutServiceResponse);
        
        /*
         * Call appropriate APIs to update Servicebase or RMA Tracker as appropriate
         */
        
        $platform = $service_providers_model->getPlatform($spId);
        
        switch($platform) {
            case 'RMA Tracker':
                $rmaResponse = $api_jobs->rmaPutJobDetails($jobId);
                if ($this->debug) $this->log("putServiceResponse : rmaPutJobDetails response :\n".varexport($rmaResponse),'samsung_api_');
                $job_model->update(array("DownloadedToSC"=>1, "JobID"=>$jobId));
                break;
            
            case 'ServiceBase':
                $api_jobs->putContactHistory($chResult['contactHistoryId']);
                $job_model->update(array("DownloadedToSC"=>1, "JobID"=>$jobId));
                break;
            
            case 'API':
            case 'Skyline':    
                $job_model->update(array("DownloadedToSC"=>0, "JobID"=>$jobId));
                break;
        }

     }
     
    /**
     * getServiceRequestItem
     * 
     * Deal with the xml related to aspecific  job, create it in Skyline and send
     * response back to Samsung
     * 
     * @param $samsungJob   The array converted from the xml relating to a specific job
     *        $user         Samsung User Name
     *        $pass         Samsung Password
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function getServiceRequestItem($samsung_job, $user, $pass) {
        $country_model = $this->loadModel('Country');
        $customer_model = $this->loadModel('Customer');
        $customer_titles_model = $this->loadModel('CustomerTitles');
        $job_model = $this->loadModel('Job');
        $manufacturers_model = $this->loadModel('Manufacturers');
        $models_model = $this->loadModel('Models');
        $network_model = $this->loadModel('ServiceNetworks');
        $service_providers_model = $this->loadModel('ServiceProviders');
        $service_types_model = $this->loadModel('ServiceTypes');
        $skyline_model = $this->loadModel('Skyline');
        $status_history_model = $this->loadModel('StatusHistory');
        $users_model = $this->loadModel('Users');
        
        if ($job_model->doesNetworkRefExist($samsung_job['ASREQUEST_INFO']['TR_NO']) == true) {
            $this->log("getServiceRequest : Job with NetworkRef (TR_NO) {$samsung_job['ASREQUEST_INFO']['TR_NO']} exists.",'samsung_api_');
            continue;
        }


        $job_fields = array();                                                  /* Array to hold fields for the job table */
        $customer_fields = array();                                             /* Array to hold field for customer tavle  */

        /*
        *  Build up fields for jobs table
        */

        $network_service_provider_array = $skyline_model->getNetworkServiceProviderAccountNo($samsung_job['ASREQUEST_INFO']['ASC_CODE']);
        if (is_null($network_service_provider_array)) {
            $msg = array(
                        'Code' => 'ERROR',
                        'Response' => "getServiceRequest : Account number (ASC_CODE) {$samsung_job['ASREQUEST_INFO']['ASC_CODE']} not found."
                        );
            $this->log($msg['Response'],'samsung_api_');
            return($msg);
        }
        $job_fields['ServiceProviderID'] = $network_service_provider_array['ServiceProviderID'];
        $job_fields['ModifiedDate'] = date('Y-m-d H:i:s');
        $job_fields['NetworkID'] = $network_model->getNetworkId($this->config['Samsung']['NetworkName']);
        $job_fields['ClientID'] = $skyline_model->getNetworkDefaultClient($job_fields['NetworkID']);
        if (is_null($job_fields['ClientID'])){
            $msg = array(
                        'Code' => "ERROR",
                        'Response' => "getServiceRequest : Cannot find client for ClientAccountNo (ASC_CODE) {$samsung_job['ASREQUEST_INFO']['ASC_CODE']} from Network {$job_fields['NetworkID']}"
                        );
            $this->log($msg['Response'],'samsung_api_');                         
            return($msg);
        } 
        $u_id = $users_model->getNetworkUserID($job_fields['NetworkID']);
        $job_fields['BookedBy'] = $u_id ;
        $job_fields['ModifiedUserID'] = $u_id ;
        $service_type_array = $service_types_model->getServiceTypeIdJobTypeIdFromName($this->config['Samsung']['ServiceType']);
        $job_fields['JobTypeID'] = $service_type_array['JobTypeID'];
        $job_fields['ServiceTypeID'] = $service_type_array['ServiceTypeID'];
        $job_fields['ManufacturerID'] = $manufacturers_model->getManufacturerId($this->config['Samsung']['ManufacturerName']);
        $job_fields['BranchID'] = $skyline_model->getClientDefaultBranch($job_fields['ClientID']);
        if (is_null($job_fields['BranchID'])){
            $msg = array(
                        'Code' => "ERROR",
                        'Response' => "getServiceRequest : No default branch set for Client {$job_fields['ClientID']}"
                        );
            $this->log($msg['Response'],'samsung_api_');
            return($msg);
        }
        $job_fields['NetworkRefNo'] = substr((string) $samsung_job['ASREQUEST_INFO']['TR_NO'],0,10);
        $job_fields['ServiceBaseModel'] = substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['PRODUCTSHORT']['MODEL_CODE'],0,40);
        $job_fields['ModelID'] = $models_model->getModelIdFromNo($samsung_job['ASREQUEST_INFO']['PRODUCT']['PRODUCTSHORT']['MODEL_CODE']);           /* Get the ID of the model */   
        if ( is_null($job_fields['ModelID']) ) {                                /* Do we have the model ID */
            unset($job_fields['ModelID']);                                      /* Not found, so don't set it */
        } else {
            unset($job_fields['ServiceBaseModel']);                             /* Have it, don't set ServiceBaseModek */
        }
        $job_fields['SerialNo'] = substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['PRODUCTSHORT']['SERIAL_NO'],0,16);
        $job_fields['DateOfPurchase'] = date( 'Y-m-d', strtotime ($samsung_job['ASREQUEST_INFO']['PRODUCT']['PRODUCTSHORT']['PURCHASE_DATE']));
        if ($job_fields['DateOfPurchase'] == '0000-00-00') unset($job_fields['DateOfPurchase']);                /* No Purchase date so don't set */
        $job_fields['Notes'] = substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['SYMPTOM']['SYMPTOM1_DESC'],0,100);
        $job_fields['Notes'] .= ", ".substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['SYMPTOM']['SYMPTOM2_DESC'],0,100);
        $job_fields['Notes'] .= ", ".substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['SYMPTOM']['SYMPTOM3_DESC'],0,100);
        $job_fields['SymptomCode'] = substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['SYMPTOM']['IRIS_SYMPTOM'],0,3);
        $job_fields['ConditionCode'] = substr((string) $samsung_job['ASREQUEST_INFO']['PRODUCT']['SYMPTOM']['IRIS_CONDITION'],0,3);
        $job_fields['GuaranteeCode'] = substr((string) $samsung_job['ASREQUEST_INFO']['SERVICE']['SERVICE_TYPE'],0,2);
        $job_fields['ExtendedWarrantyNo'] = substr((string) $samsung_job['ASREQUEST_INFO']['PACK']['PACK_NO'],0,40);
        if ( isset($samsung_job['ASREQUEST_INFO']['PACK']['SERVICE_LEVEL']) ) $job_fields['Notes'] .= 'Service Level: '.substr((string) $samsung_job['ASREQUEST_INFO']['PACK']['SERVICE_LEVEL'],0,10);
        if ( isset($samsung_job['ASREQUEST_INFO']['SAW_NO']) ) $job_fields['Notes'] .= 'SAW No: '.substr((string) $samsung_job['ASREQUEST_INFO']['SAW_NO'],0,20).' PLEASE VERIFY';
        if ( isset($samsung_job['ASREQUEST_INFO']['PANEL_VERSION']) ) $job_fields['Notes'] .= 'Panel Service: '.substr((string) $samsung_job['ASREQUEST_INFO']['PANEL_VERSION'],0,30);
        $job_fields['DateBooked'] = date( 'Y-m-d', strtotime ($samsung_job['ASREQUEST_INFO']['REQUEST_DATE']));
        $job_fields['TimeBooked'] = date( 'H:i:s', strtotime ($samsung_job['ASREQUEST_INFO']['REQUEST_TIME']));
        $job_fields['ETDDate'] = date( 'Y-m-d', strtotime ($samsung_job['ASREQUEST_INFO']['REPAIR_ETD_DATE']));
        $job_fields['ETDTime'] = date( 'H:i:s', strtotime ($samsung_job['ASREQUEST_INFO']['REPAIR_ETD_TIME']));
        $job_fields['ReportedFault'] = substr((string) $samsung_job['ASREQUEST_INFO']['INQUIRY_TEXT'],0,1000);
        if ( isset($samsung_job['ASREQUEST_INFO']['WARRANTY_LABOR']) ) $job_fields['SamsungWarrantyLabour'] = substr((string) strtoupper($samsung_job['ASREQUEST_INFO']['WARRANTY_LABOR']),0,1);
        if ( isset($samsung_job['ASREQUEST_INFO']['WARRANTY_PARTS']) ) $job_fields['SamsungWarrantyParts'] = substr((string) strtoupper($samsung_job['ASREQUEST_INFO']['WARRANTY_PARTS']),0,1);
        if ( (isset($job_fields['SamsungWarrantyLabour'])) && (isset($job_fields['SamsungWarrantyParts'])) ) {
            if ( ($job_fields['SamsungWarrantyLabour'] == 'I') && ($job_fields['SamsungWarrantyParts'] == 'O') ) {                           /* Check if part warranty */
                $job_fields['Notes'] .= "\nLABOUR ONLY CLAIM";
            } elseif ( ($job_fields['SamsungWarrantyLabour'] == 'I') && ($job_fields['SamsungWarrantyParts'] == 'O') ) {
                $job_fields['Notes'] .= "\nPARTS ONLY CLAIM";
            }
        } /* fi isset */
        if ( isset($samsung_job['ASREQUEST_INFO']['CONTACT_TRACKING']) ) $job_fields['ContactTracking'] = substr((string) $samsung_job['ASREQUEST_INFO']['CONTACT_TRACKING'],0,4);
        if ( isset($samsung_job['DEALER_INFO']['COMPANY_NAME']) ) {
            $job_fields['OriginalRetailer'] = substr((string) $samsung_job['DEALER_INFO']['COMPANY_NAME'],0,30);
        } else if ( isset($samsung_job['DEALER_INFO']['CUSTNAME']['LAST_NAME']) ) {
            $job_fields['OriginalRetailer'] = substr((string) $samsung_job['DEALER_INFO']['CUSTNAME']['LAST_NAME'],0,30);
        }
        if ( isset($samsung_job['DEALER_INFO']['ADDRESS']['CITY']) ) $job_fields['OriginalRetailer'] = substr((string) $samsung_job['DEALER_INFO']['ADDRESS']['CITY'],0,90).', ';
        if ( isset($samsung_job['DEALER_INFO']['ADDRESS']['POST_CODE']) ) $job_fields['OriginalRetailer'] .= substr((string) $samsung_job['DEALER_INFO']['ADDRESS']['POST_CODE'],0,10);
        if (isset($samsung_job['SERVICE']['SERVICE_TYPE'])) {            
            $arrRepairType = array(
                                   'IH' => 'customer',
                                   'CI' => 'customer',
                                   'PS' => 'customer',
                                   'SE' => 'customer',
                                   'SW' => 'customer',
                                   'ER' => 'customer',
                                   'SH' => 'customer',
                                   'RH' => 'customer',
                                   'KT' => 'customer',
                                   'BS' => 'customer',
                                   'SR' => 'stock',
                                   'RB' => 'stock'
                                  );
            
            $arrItemLocation = array(
                                   'IH' => 'Service Provider',
                                   'PS' => 'Service Provider',
                                   'SE' => 'Service Provider',
                                   'SW' => 'Service Provider',
                                   'ER' => 'Service Provider',
                                   'SH' => 'Service Provider',
                                   'RH' => 'Service Provider',
                                   'KT' => 'Service Provider',
                                   'CI' => 'Customer',
                                   'SR' => 'Service Provider',
                                   'RB' => 'Service Provider'
                                  );
            
            $arrJobSite = array(
                                   'IH' => 'field call',
                                   'PS' => 'field call'
                                  );
            
            if (issset($arrRepairType[$samsung_job['SERVICE']['SERVICE_TYPE']])) $job_fields['RepairType'] = $arrRepairType[$samsung_job['SERVICE']['SERVICE_TYPE']];
            if (issset($arrItemLocation[$samsung_job['SERVICE']['SERVICE_TYPE']])) $job_fields['ProductLocation'] = $arrItemLocation[$samsung_job['SERVICE']['SERVICE_TYPE']];
            if (issset($arrJobSite[$samsung_job['SERVICE']['SERVICE_TYPE']])) $job_fields['JobSite'] = $arrJobSite[$samsung_job['SERVICE']['SERVICE_TYPE']];
        }

        /*
         * Build up fields for customer table 
         */

        $customer_fields['SamsungCustomerRef'] = substr((string) $samsung_job['CONSUMER_INFO']['BP_NO'],0,10);
        $customer_fields['CustomerType'] = substr((string) $samsung_job['CONSUMER_INFO']['BP_ROLE'],0,10);
        if ( isset($samsung_job['CONSUMER_INFO']['COMPANY_NAME']) ) $customer_fields['CustomerType'] = substr((string) $samsung_job['CONSUMER_INFO']['COMPANY_NAME'],0,100);
        if ( isset($samsung_job['CONSUMER_INFO']['CUSTNAME']['TITLE']) ) $customer_fields['CustomerTitleID'] = $customer_titles_model->getCustomerTitleId($samsung_job['CONSUMER_INFO']['CUSTNAME']['TITLE']);
        if ( isset($samsung_job['CONSUMER_INFO']['CUSTNAME']['FIRST_NAME']) ) $customer_fields['ContactFirstName'] = substr((string) $samsung_job['CONSUMER_INFO']['CUSTNAME']['FIRST_NAME'],0,10);
        if ( isset($samsung_job['CONSUMER_INFO']['CUSTNAME']['LAST_NAME']) ) $customer_fields['ContactLastName'] = substr((string) $samsung_job['CONSUMER_INFO']['CUSTNAME']['LAST_NAME'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['PHONE']['TEL_NUMBER1']) ) $customer_fields['ContactHomePhone'] = substr((string) $samsung_job['CONSUMER_INFO']['PHONE']['TEL_NUMBER1'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['PHONE']['TEL_NUMBER2']) ) $customer_fields['ContactMobile'] = substr((string) $samsung_job['CONSUMER_INFO']['PHONE']['TEL_NUMBER2'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['PHONE']['TEL_NUMBER3']) ) $customer_fields['ContactWorkPhone'] = substr((string) $samsung_job['CONSUMER_INFO']['PHONE']['TEL_NUMBER3'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['PHONE']['FAX_NUMBER']) ) $customer_fields['ContactFax'] = substr((string) $samsung_job['CONSUMER_INFO']['PHONE']['FAX_NUMBER'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['PHONE']['E_MAIL']) ) $customer_fields['ContactEmail'] = substr((string) $samsung_job['CONSUMER_INFO']['PHONE']['E_MAIL'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['ADDRESS']['ADDRESS1']) ) $customer_fields['Street'] = substr((string) $samsung_job['CONSUMER_INFO']['ADDRESS']['ADDRESS1'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['ADDRESS']['ADDRESS2']) ) $customer_fields['BuildingNameNumber'] = substr((string) $samsung_job['CONSUMER_INFO']['ADDRESS']['ADDRESS2'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['ADDRESS']['ADDRESS3']) ) $customer_fields['LocalArea'] = substr((string) $samsung_job['CONSUMER_INFO']['ADDRESS']['ADDRESS3'],0,40);
        if ( isset($samsung_job['CONSUMER_INFO']['ADDRESS']['CITY']) ) $customer_fields['TownCity'] = substr((string) $samsung_job['CONSUMER_INFO']['ADDRESS']['CITY'],0,100);
        if ( isset($samsung_job['CONSUMER_INFO']['ADDRESS']['POST_CODE']) ) $customer_fields['PostalCode'] = substr((string) $samsung_job['CONSUMER_INFO']['ADDRESS']['POST_CODE'],0,10);
        if ( isset($samsung_job['CONSUMER_INFO']['ADDRESS']['COUNTRY']) ) {
            if ($samsung_job['CONSUMER_INFO']['ADDRESS']['COUNTRY'] == 'GB') $samsung_job['CONSUMER_INFO']['ADDRESS']['COUNTRY'] = 'UK';
            $customer_fields['CountryID'] = $country_model->getCountryId($samsung_job['CONSUMER_INFO']['ADDRESS']['COUNTRY']);
            if (is_null($customer_fields['CountryID'])) {                       /* Can't find country */
                unset($customer_fields['CountryID']);                           /* Unset Country ID */
                $customer_fields['TownCity'] .= ' '.(string) $samsung_job['CONSUMER_INFO']['ADDRESS']['COUNTRY'];
                $customer_fields['TownCity'] = substr($customer_fields['TownCity'],0,100);
            }
        }
        if ($samsung_job['CONSUMER_INFO']['ADDRESS']['COUNTRY'] == 'UK') {  // Check if sutoemr are in the UK to format post code
            $customer_fields['PostalCode']  = $skyline_model->ukPostCodeFormat($customer_fields['PostalCode']);
        }
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_MOBILE']) ) $customer_fields['DataProtectionMobile'] = $this->getConsentMeaning($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_MOBILE']);
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_HOME']) ) $customer_fields['DataProtectionHome'] = $this->getConsentMeaning($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_HOME']);
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_OFFICE']) ) $customer_fields['DataProtectionOffice'] = $this->getConsentMeaning($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_OFFICE']);
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_EMAIL']) ) $customer_fields['DataProtectionEmail'] = $this->getConsentMeaning($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_EMAIL']);
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_LETTER']) ) $customer_fields['DataProtectionLetter'] = $this->getConsentMeaning($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_LETTER']);
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_SMS']) ) $customer_fields['DataProtectionSMS'] = $this->getConsentMeaning($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT_SMS']);
        if ( isset($samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT']) ) {
            if ( $samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT'] == 1 ) {
                $customer_fields['DataProtection'] = 'Yes';
            } elseif ( $samsung_job['CONSUMER_INFO']['CONTACTPERMISSION']['CONTACT'] == 2 ) {
                $customer_fields['DataProtection'] = 'No';
            } else {
                $customer_fields['DataProtection'] = null;
            }
        }

        $job_fields['CustomerID'] = $skyline_model->getCustomerIdByNamePostcode(
                                                                                $customer_fields['ContactFirstName'],
                                                                                $customer_fields['ContactLastName'],
                                                                                $customer_fields['PostalCode']
                                                                                );
        if ( is_null($job_fields['CustomerID']) ) {
            $customer_create_output = $customer_model->create($customer_fields);        /* Create Customer */
            $job_fields['CustomerID'] = $customer_create_output['id'];
        } else {
            $customer_model->updateByID($customer_fields, $job_fields['CustomerID']);   /* Update customer */
        }

        $job_response = $job_model->create($job_fields);                                /* Pass these fields to the jobs model and create */
        
        /* Begin Issue 323 - Samsung Interface Should Check NonSkyline Jobs for Match - 29/04/2013 - AJW */
        /* https://bitbucket.org/pccs/skyline/issue/323/samsung-interface-should-check-nonskyline */
        if ( isset($job_fields['NetworkRefNo']) && isset($job_fields['NetworkID']) ) {              /* On NetworkRefNo and NetworkID */
            $non_skyline_job_model->JobIsNowSkylineNet($job_fields['SLNumber'], $job_fields['NetworkID'], $job_fields['NetworkRefNo']);
        } elseif ( isset($job_fields['SCJobNo']) && isset($job_fields['ServiceProviderID']) ) {     /* On Service Provider Job No and ID */
            $non_skyline_job_model->JobIsNowSkylineSp($job_fields['SLNumber'], $job_fields['ServiceProviderID'], $job_fields['SCJobNo']);
        }
        /* End Issue 323 - Samsung Interface Should Check NonSkyline Jobs for Match - 29/04/2013 - AJW */

        if ($job_response['jobId'] == 0) {                                              /* Check the JobID for the created job */
            $this->log("getServiceRequest : Record insert error. PDO Error : {$job_response['message']}",'samsung_api_');
            continue;
        }
        
        /* Status fields */
        $status_fields = array(
                               'JobID' => $job_response['jobId'],
                               'StatusID' => $skyline_model->getStatusID('02 ALLOCATED TO AGENT'),
                               'AdditionalInformation' => 'Allocated by Samsung',
                               'UserID' => $u_id,
                               'Date' => date('Y-m-d H:i:s')
                              );
        
        $status_response = $status_history_model->create($status_fields);
        
        if ($status_response['statusHistoryId'] == 0) {                                              /* Check the JobID for the created job */
            $this->log("getServiceRequest : Status Record insert error. PDO Error : {$status_response['message']}",'samsung_api_');
        }

        $this->log("getServiceRequest : NetworkRef (TR_NO) {$samsung_job['ASREQUEST_INFO']['TR_NO']} Created with Skyline JobID (SLNumber) {$job_response['jobId']}",'samsung_api_');

        /*
        * Response to Samsung
        */

        $putServiceResponse = array();                                          /* Array to create response */

        $putServiceResponse['COMPANY'] = $network_service_provider_array['CompanyCode'];
        $putServiceResponse['JOB_TYPE'] = 'SRR';
        $putServiceResponse['REQUESTER'] = $samsung_job['ASREQUEST_INFO']['ASC_CODE'];
        $putServiceResponse['TR_NO'] = $samsung_job['ASREQUEST_INFO']['TR_NO'];
        $putServiceResponse['JOB_NO'] = $job_response['jobId'];

        $site = $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['SamsungApiPath'].$this->config['Samsung']['putServiceResponse'];

        $psresponse = $this->samsungCall($putServiceResponse, $site, $user, $pass); 

        $this->log("putServiceResponse : xml Response:\n{$psresponse['raw']}",'samsung_api_');
        
        if ($psresponse['raw'] == false) {
            $this->log("xml Response empty aborting",'samsung_api_');
            $this->log(var_export($psresponse,true));
            return;
        }

        $fieldsPutServiceResponse = json_decode(json_encode($psresponse['response']),TRUE);

        $fieldsPutServiceResponse = $this->clearArray($fieldsPutServiceResponse);
        
        /*
         * Send to RMA Tracker / Servicebase
         */
        
        if ($job_fields['ServiceProviderID']) {

            $ServiceProvidersDetails = $service_providers_model->fetchRow(array("ServiceProviderID"=>$job_fields['ServiceProviderID']));

            if(is_array($ServiceProvidersDetails))
            {    
                $api = $this->loadModel('APIJobs');

                $rmaResponse = $api->rmaPutNewJob($job_response['jobId']); 


                if($rmaResponse && $rmaResponse=="SC0001")//If Rma traker call responds with success.
                {    
                    if($ServiceProvidersDetails['Platform']=="ServiceBase")
                    {
                            $api->putNewJob($job_response['jobId']); 
                    }
                    else if($ServiceProvidersDetails['Platform']=="API") {

                        $job_model->update(array("DownloadedToSC"=>0, "JobID"=>$job_response['jobId']));

                    }
                    else if($ServiceProvidersDetails['Platform']=="Website" || $ServiceProvidersDetails['Platform']=="RMA Tracker") {
                        $job_model->update(array("DownloadedToSC"=>1, "JobID"=>$job_response['jobId']));
                    }
                    $status_fields = array(
                                            'JobID' => $job_response['jobId'],
                                            'StatusID' => $skyline_model->getStatusID('03 TRANSMITTED TO AGENT'),
                                            'AdditionalInformation' => 'Allocated by Samsung',
                                            'UserID' => $u_id,
                                            'Date' => date('Y-m-d H:i:s')
                                         );

                    $status_response = $status_history_model->create($status_fields);
                }

            }

        }

        if ($fieldsPutServiceResponse['RESPONSE']['RETURN_TYPE'] == 'S') {
            $this->log("SUCCESS",'samsung_api_');
        }
    }
    
    /**
     * putServiceRequestAction
     * 
     * Get the details of a newly booked (ie service centre walk in) job and pass
     * them via the API to Samsung
     * 
     * As the Samsung API works by use of XML data in the body of the POST rather
     * than passing pareters it will use the 
     * 
     * @param $args - Arguments passed
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function putServiceRequestAction($args) {
        $country_model = $this->loadModel('Country');
        $county_model = $this->loadModel('County');
        $customer_model = $this->loadModel('Customer');
        $customer_titles_model = $this->loadModel('CustomerTitles');
        $job_model = $this->loadModel('Job');
        $models_model = $this->loadModel('Models');
        $network_model = $this->loadModel('ServiceNetworks');
        $nsp_model = $this->loadModel('NetworkServiceProvider');
        
        $jId = $args[0];
        
        $recJob = $job_model->fetchAllDetails($jId);                            /* Get Job Data */
        $recCustomer  = $customer_model->fetchRow($recJob['CustomerID']);       /* Get Customer Data */
        if (! is_int($recCustomer['CustomerTitleID'])) {                         /* Get Customer Title */
            $recTitles = array('Title' => "");
        } else {
            $recTitles = $customer_titles_model->fetchRow(array('CustomerTitleID' => $recCustomer['CustomerTitleID']));
        }
        if (! is_int($recCustomer['CountyID'])) {                                /* Get County */
            $recCounty = array('Name' => "");
        } else {
            $recCounty = $county_model->fetchRow($recCustomer['CountyID']);
        }
        if (! is_int($recCustomer['CountryID'])) {                               /* Get Country */
            $recCountry = array('Name' => "");
        } else {
            $recCountry = $country_model->fetchRow($recCustomer['CountryID']);
        }
        
        
        $recNsp = $nsp_model->fetchWhere("ServiceProviderID = {$recJob['ServiceProviderID']}
                                          AND NetworkID = {$network_model->getNetworkId($this->config['Samsung']['NetworkName'])}");
                                          
        if (is_null($recJob['ServiceProviderID'])) {                            /* Check if  no service provider (This shouldn't happen as API id for walk ins only */
            $this->log("putServiceRequest: Job {$args[0]} has no assigned ServiceProviderID",'samsung_api_');
        }
        
         /* ****************************************************
         * 
         * BE 10/09/2012
         * 
         * 11:31:18 217.34.121.180 Unexpected Error:
                        Controller = JobController
                        method = processDataAction
                        args =
                        Error = Undefined index: ExtendedWarrantyNo
                        File = /var/www/skyline/test/application/smarty/template_c/1cfbc513885480c1964848729c48e4611f133d35.file.putServiceRequest.tpl.php
                        Line = 63
                        Trace = #0 /var/www/skyline/test/application/smarty/template_c/1cfbc513885480c1964848729c48e4611f133d35.file.putServiceRequest.tpl.php(63): errorHandler(8, 'Undefined index...', '/var/www/skylin...', 63, Array)
                        #1 /var/www/skyline/test/application/smarty/libs/sysplugins/smarty_internal_template.php(432): include('/var/www/skylin...')
                        #2 /var/www/skyline/test/application/smarty/libs/sysplugins/smarty_internal_template.php(567): Smarty_Internal_Template->renderTemplate()
                        #3 /var/www/skyline/test/application/smarty/libs/Smarty.class.php(338): Smarty_Internal_Template->getRenderedTemplate()
                        #4 /var/www/skyline/test/application/controllers/SamsungClientController.class.php(439): Smarty->fetch('api/samsung/put...')
                        #5 /var/www/skyline/test/application/controllers/JobController.class.php(730): SamsungClientController->putServiceRequestAction(Array)
                        #6 /var/www/skyline/test/index.php(112): JobController->processDataAction(Array)
                        #7 /var/www/skyline/test/index.php(177): dispatch('JobController', 'processDataActi...', Array)
                        #8 {main} 
         * 
         * NOTE: Cannot find ExtendedwarrantyNo in Database so temporarily 
         *       fixing this problem by setting smarty value to blank.
         * 
         ******************************************************/
        $recJob['ExtendedWarrantyNo'] =  '';
       
        $this->smarty->assign('job',$recJob);
        $this->smarty->assign('country', $recCountry);
        $this->smarty->assign('county', $recCounty);
        $this->smarty->assign('customer', $recCustomer);
        $this->smarty->assign('customer_title', $recTitles);
        $this->smarty->assign('nsp',$recNsp[0]); 
        
        if (is_null($recJob['ModelID'])) {
            $this->smarty->assign('model_code',$recJob['ServiceBaseModel']);    /* No model ID so return service base model */
        } else {
            $recModel = $models_model->fetchRow(array('ModelID' => $recJob['ModelID']));    /* Model ID so get model detials */
            $this->smarty->assign('model_code',$recModel['ModelNumber']);       /* Output Model number based on ModelID */
        }
        $consent = array (                                                      /* Data conesnt - need to convert db enum to Samsung integer */
                          'DataProtectionMobile' => $this->putConsentMeaning($recCustomer['DataProtectionMobile']),
                          'DataProtectionEmail' => $this->putConsentMeaning($recCustomer['DataProtectionEmail']),
                          'DataProtectionHome' => $this->putConsentMeaning($recCustomer['DataProtectionHome']),
                          'DataProtectionOffice' => $this->putConsentMeaning($recCustomer['DataProtectionOffice']),
                          'DataProtectionSMS' => $this->putConsentMeaning($recCustomer['DataProtectionSMS']),
                          'DataProtectionLetter' => $this->putConsentMeaning($recCustomer['DataProtectionLetter'])
                         );
        $this->smarty->assign('consent', $consent);
        
        $xml_code = $this->smarty->fetch('api/samsung/putServiceRequest.tpl'); 
        
        $response = $this->samsungCall(
                                       $xml_code, 
                                       $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['SamsungApiPath'].$this->config['Samsung']['putServiceRequest'],
                                       $recNsp[0]['TrackingUsername'], 
                                       $recNsp[0]['TrackingPassword']
                                      );
        $this->log("putServiceRequest : $jId : xml Response:\n{$response['raw']}",'samsung_api_');
        
        $fieldsPutServiceResponse = json_decode(json_encode($response['response']),TRUE);
        
        if ($fieldsPutServiceResponse['ASREQUEST_RESPONSE']['RETURN_TYPE'] == 'S') {
            $this->log("putServiceRequest : $jId : SUCCESS",'samsung_api_');
            
            $job_model->update(                                                 /* Update Job with Samsung reference number */
                                array(
                                      'JobID' => $jId,
                                      'NetworkRefNo' => $fieldsPutServiceResponse['ASREQUEST_RESPONSE']['TR_NO']
                                     )
                               );
            
            $customer_model->updateByID(                                        /* Update Customer with Samsung reference number */
                                        array('SamsungCustomerRef' => $fieldsPutServiceResponse['ASREQUEST_RESPONSE']['CUSTOMER_NO']),
                                        $recJob['CustomerID']
                                       );
        } else {
           $this->log("putServiceRequest : $jId : FAIL",'samsung_api_');
        }
    }
    
    /**
     * putRepairStatusAction
     * 
     * Update Samsung with the details of the jobs whcich have changed throught 
     * the life of the job.
     *  
     * @param $args - Arguments passed
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function putRepairStatusAction($args) {
        $jId = $args[0];
        
        $appointments_model = $this->loadModel('Appointment');
        $job_model = $this->loadModel('Job');
        $models_model = $this->loadModel('Models');
        $network_model = $this->loadModel('ServiceNetworks');
        $nsp_model = $this->loadModel('NetworkServiceProvider');
        $part_model = $this->loadModel('Part');
        //$status_history_model = $this->loadModel('StatusHistory');
        
        $recJob = $job_model->fetchAllDetails($jId);                            /* Get Job Data */
        $recNsp = $nsp_model->fetchWhere("ServiceProviderID = {$recJob['ServiceProviderID']}
                                          AND NetworkID = {$network_model->getNetworkId($this->config['Samsung']['NetworkName'])}");
                                          
        if (is_null($recJob['SamsungWarrantyLabour'])) $recJob['SamsungWarrantyLabour'] = 'I';    /* Change request by Neil Wrightling - Deafult to I */
        if (is_null($recJob['SamsungWarrantyParts'])) $recJob['SamsungWarrantyParts'] = 'I';      /* See Issue 129 - https://bitbucket.org/pccs/skyline/issue/129/putrepairstatus-samsung-upload-issues */                                         
                                          
        $lastStatus = $job_model->getLastStatus($jId);
        
        if (                                                                    /* If status is ST025 we need to write current date/time as Engimneer Assigned */
            ($lastStatus == '07 UNIT IN REPAIR')
            || ($lastStatus == '09 ESTIMATE ACCEPTED')
            || ($lastStatus == '17 AUTHORITY ISSUED')
            || ($lastStatus == '18 FIELD CALL COMPLETE')
            || ($lastStatus == '32 INFO RECEIVED')    
           ) {
            $eng_assign_date = date('Y-m-d');
            $eng_assign_time = date('H:i:s');
            
            $job_model->update(
                               array(
                                     'JobID' => $jId,
                                     'EngineerAssignedDate' => $eng_assign_date,
                                     'EngineerAssignedTime' => $eng_assign_time
                                    )
                              );
            
            $this->smarty->assign('eng_assign_date',$eng_assign_date);
            $this->smarty->assign('eng_assign_time',$eng_assign_time);
        } else {                                                                /* Otherwise use cuurent entries of Engineer Assigned Date and Time */
            $this->smarty->assign('eng_assign_date',$recJob['EngineerAssignedDate']);
            $this->smarty->assign('eng_assign_time',$recJob['EngineerAssignedTime']);
        }
        
        $this->smarty->assign('job',$recJob);
        $this->smarty->assign('nsp',$recNsp[0]);
        if (is_null($recJob['ModelID'])) {
            $this->smarty->assign('model_code',$recJob['ServiceBaseModel']);    /* No model ID so return service base model */
        } else {
            $recModel = $models_model->fetchRow(array('ModelID' => $recJob['ModelID']));    /* Model ID so get model detials */
            $this->smarty->assign('model_code',$recModel['ModelNumber']);       /* Output Model number based on ModelID */
        }
        
        $tr_status = array();
        $tr_reason = array();
        
        switch ($lastStatus) {
            case '01 UNALLOCATED':
                    $tr_status[0] = 'ST010';
                    $tr_reason[0] = '';
                    break;
                
            case '02 ALLOCATED TO AGENT':
                    $tr_status[0] = 'ST010';
                    $tr_reason[0] = '';
                    break;
                
            case '03 TRANSMITTED TO AGENT':
                    $tr_status[0] = 'ST010';
                    $tr_reason[0] = '';
                    break;

            case '04 JOB VIEWED BY AGENT':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HA005';
                    break;

            case '05 FIELD CALL REQUIRED':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HA005';
                    break;

            case '06 FIELD CALL ARRANGED':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HAZ04';
                    break;

            case '07 UNIT IN REPAIR':
                    $tr_status[0] = 'ST025';
                    $tr_reason[0] = 'HAZ01';
                    break;

            case '08 ESTIMATE SENT':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP030';
                    break;

            case '09 ESTIMATE ACCEPTED':
                    $tr_status[0] = 'ST025';
                    $tr_reason[0] = 'HEZ01';
                    break;

            case '10 ESTIMATE REFUSED':
                    $tr_status[0] = 'ST052';
                    $tr_reason[0] = 'HCZ04';
                    break;

            case '11 PARTS REQUIRED':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HPZ02';
                    break;

            case '12 PARTS ORDERED':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HPZ02';
                    break;

            case '13 PARTS RECEIVED':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HPZ03';
                    break;

            case '14 2ND FIELD CALL REQ.':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP015';
                    break;

            case '15 2ND FIELD CALL BKD.':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP015';
                    break;

            case '16 AUTHORITY REQUIRED':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP035';
                    break;

            case '17 AUTHORITY ISSUED':
                    $tr_status[0] = 'ST025';
                    $tr_reason[0] = 'HEZ01';
                    break;

            case '18 REPAIR COMPLETED':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HL005';
                    } elseif ( $recJob['CompletionStatus'] != 'PARTS UNAVAILABLE' ) {
                        $tr_status[0] = 'ST040';
                        $tr_reason[0] = 'HGZ01';
                    } else {
                        $tr_status[0] = 'ST070';
                        $tr_status[0] = '';
                    }
                    break;

            case '18 FIELD CALL COMPLETE':
                    $tr_status[0] = 'ST025';
                    $tr_reason[0] = 'HEZ01';
                    break;

            case '19 DELIVER BACK REQ.':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HL015';
                    } else {
                        $tr_status[0] = 'ST070';
                        $tr_reason[0] = '';
                    }
                    break;

            case '20 DELIVER BACK ARR.':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HGZ01';
                    } elseif ( $recJob['CompletionStatus'] != 'PARTS UNAVAILABLE' ) {
                        $tr_status[0] = 'ST040';
                        $tr_reason[0] = 'HGZ01';
                    } else { 
                        $tr_status[0] = 'ST070';
                        $tr_reason[0] = '';
                    }
                    break;

            case '21C INVOICED':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HL005';
                    } elseif ( $recJob['CompletionStatus'] != 'PARTS UNAVAILABLE' ) {
                        $tr_reason[0] = 'HGZ01';
                        $tr_status[0] = 'ST040';
                    } else {
                        $tr_status[0] = 'ST070';
                        $tr_reason[0] = '';
                    }
                    break;

            case '21W CLAIMED':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HL005';
                    } elseif ( $recJob['CompletionStatus'] != 'PARTS UNAVAILABLE' ) {
                        $tr_reason[0] = 'HGZ01';
                        $tr_status[0] = 'ST040';
                    } else {
                        $tr_status[0] = 'ST070';
                        $tr_reason[0] = '';
                    }
                    break;

            case '25 INVOICE PAID':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HL005';
                    } elseif ( $recJob['CompletionStatus'] != 'PARTS UNAVAILABLE' ) {
                        $tr_status[0] = 'ST040';                    
                        $tr_reason[0] = 'HGZ01';
                    } else {
                        $tr_status[0] = 'ST070';
                        $tr_reason[0] = '';
                    }
                    break;

            case '26 INVOICE RECONCILED':
                    if ( is_null($recJob['ClosedDate']) ) {
                        $tr_status[0] = 'ST035';
                        $tr_reason[0] = 'HL005';                
                    } elseif ( $recJob['CompletionStatus'] != 'PARTS UNAVAILABLE' ) {
                        $tr_status[0] = 'ST040';
                        $tr_reason[0] = 'HGZ01';
                    } else {
                        $tr_status[0] = 'ST070';
                        $tr_reason[0] = '';
                    }
                    break;

            case '29 JOB CANCELLED':
                    $tr_status[0] = 'ST052';
                    switch ($recJob['CompletionStatus']) {
                        case 'WRONG MANUFACTURER':
                            $tr_reason[0] = 'HC015';
                            break;

                        case 'CUSTOMER NOT CONTACTABLE':
                            $tr_reason[0] = 'HC025';
                            break;

                        case 'REQUEST BY CUSTOMER':
                            $tr_reason[0] = 'HC030';
                            break;

                        case 'EXCHANGED':
                            $tr_reason[0] = 'HCZ08';
                            break;

                        case 'NO FAULT FOUND':
                            $tr_reason[0] = 'HC005';
                            break;

                        case 'DUPLICATE JOB':
                            $tr_reason[0] = 'HCZ01';
                            break;

                        case 'PARTS ISSUE':
                            $tr_reason[0] = 'HCZ24';
                            break;

                        case 'RESCHEDULE':
                            $tr_reason[0] = 'HCZ03';
                            break;

                        case 'COST ISSUE':
                            $tr_reason[0] = 'HCZ04';
                            break;

                        case 'WRONG ASC':
                            $tr_reason[0] = 'HC015';
                            break;
                    } /* esac $recJob['CompletionStatus'] */
                    break;

            case '30 OUT CARD LEFT':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP025';
                    break;

            case '31 WAITING FOR INFO':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP010';
                    break;

            case '32 INFO RECEIVED':
                    $tr_status[0] = 'ST025';
                    $tr_reason[0] = 'HEZ01';
                    break;

            case '33 PART NO LONGER AVAILABLE':
                    $tr_status[0] = 'ST030';
                    $tr_reason[0] = 'HP040';
                    break;

            case '34 PACKAGING SENT':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HA010';
                    break;

            case '39 CX NOT AVAIL - MESSAGE LEFT':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HAZ05';
                    break;

            case '40 CX NOT AVAIL - NO RESPONSE':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HAZ06';
                    break;

            case '41 CX NOT AVAIL - WRONG NUM':
                    $tr_status[0] = 'ST015';
                    $tr_reason[0] = 'HAZ07';
                    break;
                
            default:
                    $tr_status[0] = '';
                    $tr_reason[0] = '';
             
        } /* esac $lastStatus */
        
        $this->smarty->assign('tr_status',$tr_status);
        $this->smarty->assign('tr_reason',$tr_reason);
        
        $appointment = $appointments_model->getNextAppointment($jId);
        
        if (is_null($appointment)) {
            $appointment['AppointmentDate'] = '';                               /* No appointments so assign blank */
            $appointment['AppointmentTime'] = '';
        }
        
        $this->smarty->assign('appointment',$appointment);
        
        $recPart = $part_model->getNoneOtherAdjustmentByJobId($jId);
        
        if ( count($recPart) > 0 ) {       
            foreach ($recPart as $n => $part) {
                if(! is_null($part) ) {
                    if ( 
                        ((is_null($part['ReceivedDate'])) || ($part['ReceivedDate'] == 0))
                        && ((! is_null($part['OrderDate'])) || ($part['OrderDate'] == 0))
                        && (
                                ($tr_reason[0] == 'HPZ02')
                                || ($tr_reason[0] == 'HP040')
                                || ($tr_reason[0] == 'HP050')
                           )
                    ) {
                        $recPart[$n]['status'] = 'P';
                    } else {
                        $recPart[$n]['status'] = 'U';
                    }
                    
                    if($recPart[$n]['SupplierOrderNo'] == 0) {                  /* If supplier order number is 0 */
                        $recPart[$n]['SupplierOrderNo'] = '';                   /* Clear the number */
                    }
                } /* fi !is_null part */
            } /* next recPart */
        } /* fi count recPart > 0 */
        
        $this->smarty->assign('parts', $recPart);
        
        $xml_code = $this->smarty->fetch('api/samsung/putRepairStatus.tpl');    /* Generate XML code */
        
        $response = $this->samsungCall(
                                       $xml_code, 
                                       $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['SamsungApiPath'].$this->config['Samsung']['putRepairStatus'],
                                       $recNsp[0]['TrackingUsername'], 
                                       $recNsp[0]['TrackingPassword']
                                      );
        $this->log("putServiceRequest : $jId : xml Response:\n{$response['raw']}",'samsung_api_');
        
        $fieldsPutServiceResponse = json_decode(json_encode($response['response']),TRUE); 
        
        if (isset($fieldsPutServiceResponse['RESPONSE']['RETRUN_TYPE'])) {
            $fieldsPutServiceResponse['RESPONSE']['RETURN_TYPE'] = $fieldsPutServiceResponse['RESPONSE']['RETRUN_TYPE'];
        }
        
        if($fieldsPutServiceResponse['RESPONSE']['RETURN_TYPE'] == 'S' ) {
            $this->log("putRepair : $jId : SUCCESS",'samsung_api_');
        } else {
            $this->log("putRepair : $jId : FAIL",'samsung_api_');
        }
    }
    
    /**
     * WLCAction
     * 
     * Update Samsung with details of Warranty Claim
     *  
     * @param $args - Arguments passed
     *                First argument is Job ID
     *                Second argument is optional
     *                      - Blank for Post_WLC
     *                      - BillNumber for Change_WLC
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function WLCAction($args) {
        $jId = $args[0];
        if ( isset($args[1]) ) {
            $bill_number = $args[1];
        } else {
            $bill_number = '';
        }
        
        $country_model = $this->loadModel('Country');
        $customer_model = $this->loadModel('Customer');
        $customer_titles_model = $this->loadModel('CustomerTitles');
        $job_model = $this->loadModel('Job');
        $models_model = $this->loadModel('Models');
        $network_model = $this->loadModel('ServiceNetworks');
        $nsp_model = $this->loadModel('NetworkServiceProvider');
        $parts_model = $this->loadModel('Part');
        $cr_model = $this->LoadModel('ClaimResponse');
        
        $recJob = $job_model->fetchAllDetails($jId);                            /* Get Job Data */
        $recNsp = $nsp_model->fetchWhere("ServiceProviderID = {$recJob['ServiceProviderID']}
                                          AND NetworkID = {$network_model->getNetworkId($this->config['Samsung']['NetworkName'])}");
        $recCustomer = $customer_model->fetchRow($recJob['CustomerID']);
        $recCountry = $country_model->fetchRow(array('CountryID' => $recCustomer['CountryID']));
        $recCustomerTitles = $customer_titles_model->fetchRow(array('CustomerTitleID' => $recCustomer['CustomerTitleID']));
        $title = strtoupper($recCustomerTitles['Title']);
        if ( 
            ( $title == 'MR')
            OR ( $title == 'MRS')
            OR ( $title == 'MS')
           ) {
            $title .= '.';
        } else if ( $title != 'MISS' ) {
            $title = 'NON';
        }
    
        
        $comma = strpos($recCustomer['LocalArea'],',');
        if ($comma === false) {
            $recCustomer['Address2'] = $recCustomer['LocalArea'];
            $recCustomer['Address3'] = '';
        } else {
            $recCustomer['Address2'] = substr($recCustomer['LocalArea'],0,$comma-1);
            $recCustomer['Address3'] = substr($recCustomer['LocalArea'],$comma+1);
        }
        
        if (! is_null($recJob['ModelID'])) {                                    /* Check if a model Id is availible */
            $model = $recJob['ServiceBaseModel'];                               /* No so use the service base model */
        } else {
            $recModel = $models_model->fetchRow(array('ModelID' => $recJob['ModelID']));    /* Yes so get the model Number from the model ID. */
            $model = $recModel['ModelNo'];
        }
        
        $iris = $parts_model->getIRISCode($jId);
        $totalUnitCost = $parts_model->getTotalUnitCost($jId);
        $otherCost = $parts_model->getOtherCost($jId);
        $additionalFreight = $parts_model->hasAdditionalFreight($jId);
        $partsLine = $parts_model->GetSamsungPartsLineList($jId);

        $this->smarty->assign('bill_number', $bill_number);                     /* Post WLC  has no bill number */
        $this->smarty->assign('job', $recJob);
        $this->smarty->assign('nsp', $recNsp[0]);
        $this->smarty->assign('customer', $recCustomer);
        $this->smarty->assign('country', $recCountry);
        $this->smarty->assign('title', $title);
        $this->smarty->assign('model', $model);
        $this->smarty->assign('iris', $iris);
        $this->smarty->assign('total_unit_cost', $totalUnitCost);
        $this->smarty->assign('other_cost', $otherCost);
        $this->smarty->assign('parts_lines', $partsLine);
        
        if ($additionalFreight) {
            $this->smarty->assign('additional_freight', 'Y');
        } else {
            $this->smarty->assign('additional_freight', 'N');
        }
        
        $xml_code = $this->smarty->fetch('api/samsung/WLC.tpl');           /* Generate XML code */
        
        //echo "<pre>".htmlentities($xml_code)."</pre>";
        
        $response = $this->samsungCall(
                                       $xml_code, 
                                       $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['PostWLC'],
                                       $recNsp[0]['WarrantyUsername'], 
                                       $recNsp[0]['WarrantyPassword']
                                      );
        $this->log("Post_WLC : $jId : xml Response:\n{$response['raw']}",'samsung_api_');
        
        $postWlcResponse = json_decode(json_encode($response['response']),TRUE);
        $postWlcResponse = $this->clearArray($postWlcResponse);
        
        if ($this->debug) $this->log('postWlcResponse: '.var_export($postWlcResponse,true));
        if ( isset($postWlcResponse['ClaimNumber']) ) {
            $this->log("Post_WLC : $jId : SUCCESS",'samsung_api_');
            
            $job_model->update(array('JobID' => $jId, 'ClaimBillNumber' => $postWlcResponse['BillNumber']));
            
            foreach ($postWlcResponse['DetailLineList'] as $part) {
                $parts_model->updateAcceptedCost($part['DetailLineItem']['PartNo'], intval($part['DetailLineItem']['UnitCost'])/100);
            }
            
            foreach ($postWlcResponse['ErrorDetailLineList'] as $error) {
                $cr_model->create(
                                   array(
                                         'JobID' => $jId,
                                         'ErrorCode' => $error['ErrorDetailLine']['ErrorCode'],
                                         'ErrorDescription' => $error['ErrorDetailLine']['ErrorDescription']
                                         )
                                  );
            }
        } else {
            $this->log("Post_WLC : $jId : FAIL",'samsung_api_');
            
            if ( isset ($postWlcResponse['ErrorDetailLineList']) ) {            /* If we have an error line */
                foreach ($postWlcResponse['ErrorDetailLineList'] as $error) {   /* Record it */ 
                    $scr_model->create(
                                    array(
                                            'JobID' => $jId,
                                            'ErrorCode' => $error['ErrorCode'],
                                            'ErrorDescription' => $error['ErrorDescription']
                                            )
                                    );
                }
            }
        }
          
    }
    
    /**
     * getConsentMeaning
     * 
     * Get the meaning of Samsung data consent values
     * 
     * @param integer $type     The Samsung Data Consent Value (1,2,3)
     * 
     * @return string   The description of the consent value for the database
     *                  enum field
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function getConsentMeaning($type) {
        $values = array (
                         1 => 'Marketing',
                         2 => 'SVCCampaign',
                         3 => 'Both'
                        );
        
        return($values[$type]);
    }
    
    /**
     * putConsentMeaning
     * 
     * Put the meaning of Samsung Data consent values from the dtabase enum
     * 
     * @param string $type     The data conest value stored in the database
     * 
     * @return integer   The Samsung Data Consent Value (1,2,3) 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    public function putConsentMeaning($type) {
        switch($type) {
            case 'Marketing':
                $retval = 1;
                break;
            
            case 'SVCCampaign':
                $retval = 2;
                break;
            
            case 'Both':
                $retval = 3;
                break;
            
            default:
                $retval = null;
                break;
        }
        
        return($retval);
    }
    
    /**
     * clearArray
     * 
     * Clear out emply arrays for blank values that appear in the format return 
     * by Samsung
     * 
     * @param array $arr    The array to be cleared
     * 
     * @return array    Array with blank values removed
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    private function clearArray($arr) {
        $retVal = array();
               
        foreach ($arr as $k=>$v) {
            
            if (is_array($arr[$k])) {
                if (count($arr[$k]) == 0) {
                    $retVal[$k] = null;
                }
                else
                {
                    $retVal[$k] = $this->clearArray($arr[$k]);
                } /* fi count */
            } else {
                $retVal[$k] = $arr[$k];
            } /* fi is_array */
        } /* next array */
        
        return($retVal);
    }
    
    /**
     * samsungCall
     * 
     * Exceute call to Samsung API
     * 
     * @param array $args - Array of the arguments to be turned in XML
     *                      (or string of generated XML)
     *        string $url - URL to be called
     *        string $user - Username for API
     *        string $pass - Password for API
     * 
     * @return array   Curl response information and information retuned by API
     * 
     * @author Andrew Williams <a.williams@pccsuk.com> 
     **************************************************************************/
    
    protected function samsungCall($args, $url, $user, $pass) {
        
        $ch = curl_init();
        
        if (is_array($args)) {
            $xml_build['ASC_JOB_REQUEST']['JOB_REQUEST'] = $args;

            $xml = $this->SerializeAsXML($xml_build);                           /* Get  XML from array */
        } else {
            $xml = $args;                                                       /* XML sent as parmeter */
        }
        
        $this->log("XML sent out to $url : \n $xml","samsung_api_");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: text/xml',
            'Content-Length: '.strlen ($xml)
        ));

        curl_setopt($ch, CURLOPT_HEADER, 0);

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $user . ':' . $pass);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array ('Accept: ' . $this->acceptType));*/



        $response = curl_exec($ch);         
        $info = curl_getinfo($ch);


        curl_close($ch);

        //$this->log(var_export($response,true));

        $raw = $response;                                                   /* Store response, (used in some specific API logs) */
        /*$this->log(var_export($info,true));*/
        if ($this->debug) $this->log(var_export($raw,true));
        if (is_array($info)) {
            if (stripos($info['content_type'],'json') !== false) {
                $response = json_decode($response,true);
            } else if (stripos($info['content_type'],'xml') !== false) {
                $response = preg_replace( "/[^\x0A\x0D\x20-\x7E]/", "", $response );
                $response = (array) simplexml_load_string($response);
            }
        }

        return array ( 'response' => $response, 
                        'info' => $info,
                        'raw' => $raw);

        
    }   
    
    
    /**
     * Description
     * 
     * Create CML from an object or Array
     * 
     * @param array / object $obj  Array or object to turn to xeml
     *        integer $depth       Optional varible to specify how deep to
     *                             traverse
     * 
     * @return array recordSetSerialise   Associative array containing respose
     * 
     * @author Brian Etherington <b.etherington@pccsuk.com>  
     **************************************************************************/
    protected function SerializeAsXML($obj, $depth = 0) {
        
        $d = '';
        
        if (is_array($obj) || is_object($obj)) {
            
            $tabs = '';
            
            for ($i = 0; $i <= $depth; $i++) {
                $tabs .= "\t";
            }
            
            foreach ($obj as $o_i => $o_v) {
                
                $useTab = false;
                /* If element is numeric then ignore as xml tags can not be numeric. Data being passed should have been through recordSetSerialise when building response */
                if (!is_int($o_i)) {
                    /*Not numeric - so use tag */               
                    $d .= $tabs . "<{$o_i}>";
                }
                if (is_array($o_v) || is_object($o_v)) {
                    $d .= "\n" . $this->SerializeAsXML($o_v, ($depth + 1));
                    $useTab = true;
                } else {
                    $d .= htmlentities($o_v, ENT_COMPAT);
                }
                if ($useTab) {
                    $d .= $tabs;
                }
                if (!is_int($o_i)) {
                    /*Not numeric - so use tag */    
                    $d .= "</{$o_i}>\n";
                }
            }
            
        }
        
        return $d;
    }
    
    
    /**
     * recordSetSerialise
     * 
     * Serialise a recordset so ecah record is under number=>tag for easy
     * xml construction
     * 
     * @param array $recordset     Recordset from DbQuery, 
     *        string $tag          Tag to surround each record        
     * 
     * @return array    Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function recordSetSerialise($recordset, $tag) {
        $response = array();
        
        foreach ($recordset as  $n => $field) {
            $response[$n][$tag] = $field;
        }
        return($response);
    }
    
    
     /**
     * putStockPartOrderReqAction
     * 
     * Create stock order on gspn for service provider
     * 
     * 
     * As the Samsung API works by use of XML data in the body of the POST rather
     * than passing pareters it will use the 
     * 
     * @param $args - Arguments passed
     * 
     * @return 
     * 
     * @author Andris Polnikovs <a.polnikovs@pccsuk.com> 
     **************************************************************************/
    public function putStockPartOrderReqAction($args) {
        $spID=$this->user->ServiceProviderID;
       
        $nsp_model = $this->loadModel('NetworkServiceProvider');
        $network_model = $this->loadModel('ServiceNetworks');
        $stock_model = $this->loadModel('Stock');
        $serviceProviders_model = $this->loadModel('ServiceProviders');
        $companyName=$serviceProviders_model->getServiceCentreCompanyName($spID);
        
        $recNsp = $nsp_model->fetchWhere("ServiceProviderID =$spID
                                          AND NetworkID = {$network_model->getNetworkId($this->config['Samsung']['NetworkName'])}");
        
        //getting all template data from id
          $partData=$stock_model->getAllPartData($_POST['partID']*1);
          $this->log($partData,"partLog");
        //  print_r($partData);
          if($partData['PartStatusID']==2){//check if status is 02 order required
        $this->smarty->assign('partData',$partData);                                  
        $this->smarty->assign('nsp',$recNsp[0]); 
        $this->smarty->assign('companyName',$companyName);
        
        $xml_code = $this->smarty->fetch('api/samsung/putStockPartOrder.tpl'); 
        $this->log("---------------------------------------Start--------------------------------------------","gspnResp");
        $this->log($xml_code,"gspnResp");
        $this->log($this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['putStockOrderPath'],"gspnResp");
        $this->log( $recNsp[0]['WarrantyUsername'],"gspnResp");
        $this->log( $recNsp[0]['WarrantyPassword'],"gspnResp");
        $response = $this->samsungCall(
                                       $xml_code, 
                                       $this->config['Samsung']['SamsungApiUrl'].$this->config['Samsung']['putStockOrderPath'],
                                       $recNsp[0]['WarrantyUsername'], 
                                       $recNsp[0]['WarrantyPassword']
                                      );
       $this->log($response,"gspnResp");
       $response=json_decode(json_encode($response['response']),TRUE);
       $this->log($response,"gspnResp");
       if(isset($response['Response']['ResponseStatus'])&&$response['Response']['ResponseStatus']=='S'){
           //if order created on gspn update order number on parts and order records
           $stock_model->setSamsungOrderNo($response['OrderResult']['SalesOrderNumber'],$partData['OrderNo'],$partData['PartID'],$response['OrderResult']['ItemList']['Etd']);
       }
        echo json_encode($response);

          }
    }
    
}

?>
