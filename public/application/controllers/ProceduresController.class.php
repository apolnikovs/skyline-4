<?php

require_once('CustomSmartyController.class.php');


/**
 * Description
 *
 * This class handles all actions of Procedures & Scheduled Tasks under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.00
 *   
 * Changes
 * Date        Version Author                Reason
 * 27/09/2012  1.00    Nageswara Rao Kanteti Initial Version
 ******************************************************************************/

class ProceduresController extends CustomSmartyController {
    
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    public $SkylineBrandID=1000;
   
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
        //$this->smarty->assign('_theme', 'skyline');
       
       
       if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
	    $this->smarty->assign('loggedin_user', $this->user);

            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;

        } else {
            
            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index',null, null);

        } 
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
        
        
        
        
       
    }
       
    
     /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     * 
     *  
     **************************************************************************/
     public function ProcessDataAction($args)
     {
            $modelName = isset($args[0])?$args[0]:'';
            $type      = isset($args[1])?$args[1]:'';
         
            if($modelName && $modelName!='')
            {
                
                
                $this->page =  $this->messages->getPage(lcfirst($modelName), $this->lang);
                
                
                $model  = $this->loadModel($modelName);
                
                
                if($type=='fetch')
                {
                    $_POST['firstArg']  = isset($args[2])?$args[2]:'';
                    $_POST['secondArg'] = isset($args[3])?$args[3]:'';
                    $_POST['thirdArg'] = isset($args[4])?$args[4]:'';
                    
                    $result     =   $model->fetch($_POST);
                }
                else
                {
                   // $this->log(var_export($_FILES, true));
                    $result     = $model->processData($_POST);
                }    
                echo json_encode( $result );
           }
            return;
     }
    
     
     
      public function indexAction( /*$args*/ ) { 
          
          
      }
      
      
     
    
}

?>
