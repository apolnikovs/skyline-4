/***********************************************************************
 * 
 *  jquery UI combobox
 *  http://http://jqueryui.com/demos/autocomplete
 *  
 *  Brian Etherington
 *  
 *  31/08/2012 modified to add change event.
 *  
 *  Example:
 *  
 *  $('#myCombobox').combobox({ change: function() { alert('change event....'); },
 *                              selected: function() { alert('selected event...');  } });
 * 
 * 
 ***********************************************************************/

(function( $ ) {
		$.widget( "ui.combobox", {
			_create: function() {
				var input,
					self = this,
					select = this.element.hide(),
					selected = select.children( ":selected" ),
					value = selected.val() ? selected.text() : "",
					wrapper = this.wrapper = $( "<span>" )
						.addClass( "ui-combobox" )
						.insertAfter( select );

				input = $( "<input>" )
					.appendTo( wrapper )
					.val( value )
					.addClass( "ui-state-default ui-combobox-input" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: function( request, response ) {
							var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
							response( select.children( "option" ).map(function() {
								var text = $( this ).text();
								if ( this.value && ( !request.term || matcher.test(text) ) )
									return {
										label: text.replace(
											new RegExp(
												"(?![^&;]+;)(?!<[^<>]*)(" +
												$.ui.autocomplete.escapeRegex(request.term) +
												")(?![^<>]*>)(?![^&;]+;)", "gi"
											), "$1" ),  //<strong>$1</strong>
										value: text,
										option: this
									};
							}) );
						},
						select: function( event, ui ) {
                                                    
							//ui.item.option.selected = true;
                                                        // Brian: 17/10/2012
                                                        // fix for "well known ie6 bug" 
                                                        // ie6 errors when setting a selected <select> option
                                                        
                                                        if(typeof document.body.style.maxHeight === "undefined") {
                                                           
                                                            // do ie6 stuff here....                                                                
                                                            try {
                                                                ui.item.option.selected = true; 
                                                            } catch (err) {
                                                                // ignore error
                                                                // ie6 appears to make the assignment 
                                                                // successfully anyway despite the error!!!!
                                                            }                                                          
                                                            
                                                        } else {
                                                            
                                                            // normal browsers here....
                                                            ui.item.option.selected = true;

                                                        }
                                                    
                                                        self._trigger( "selected", event, {
								item: ui.item.option
                                                        }); 

                                                        //Brian: add on change callback event
                                                        self._trigger( "change", event, {
								item: ui.item.option
                                                        });
                                                            
						},
						change: function( event, ui ) {
                                                    
							if ( !ui.item ) {
								var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( $(this).val() ) + "$", "i" ),
									valid = false;
								select.children( "option" ).each(function() {
									if ( $( this ).text().match( matcher ) ) {
										this.selected = valid = true; 
                                                                                
                                                                                //Brian: add on change callback event
                                                                                self._trigger( "change", event, {
                                                                                    item: ui
                                                                                });
										return false;
									}
								});
                                                                
                                                               if(valid)
                                                                {   
                                                                    //Nag: Assigning same text to text box value. 
                                                                    $(this).val(select.children( "option:selected" ).text());
                                                                }
                                                                
								if ( !valid ) {
									// remove invalid value, as it didn't match anything
									$( this ).val( "" );
									select.val( "" );
									input.data( "autocomplete" ).term = "";
									return false;
								}                                                                 
							}
							
						}
					})
					.addClass( "ui-widget ui-widget-content ui-corner-left" );

				input.data( "autocomplete" )._renderItem = function( ul, item ) {
					return $( "<li></li>" )
						.data( "item.autocomplete", item )
						.append( "<a>" + item.label + "</a>" )
						.appendTo( ul );
				};
                                
				$( "<a>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.appendTo( wrapper )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "ui-corner-right ui-combobox-toggle" )
					.click(function() {
						// close if already visible
						if ( input.autocomplete( "widget" ).is( ":visible" ) ) {
							input.autocomplete( "close" );
							return;
						}

						// work around a bug (likely same cause as #5265)
						$( this ).blur();

						// pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
						input.focus();
					});
			},

			destroy: function() {
				this.wrapper.remove();
				this.element.show();
				$.Widget.prototype.destroy.call( this );
			}
		});
	})( jQuery );

       /**
        * Description
        * 
        * This method is used for to search the value in select box and assigns the its html value to the text box generated jquery.
        *
        * @param string $ElementID  select box id
        * @param string $ElementNo  element position 
        * 
        * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
        */
        function setValue($ElementID, $ElementNo)
        {
            
               $ElementNo = typeof $ElementNo == 'undefined' ? ":first" : $ElementNo
               
              

            
                $ParentID = $($ElementID).parent().attr('id')
                
                if($($ElementID).val()!='' && $($ElementID).val()!='0')
                    {
                        $text =  $($ElementID+" option[value='"+$($ElementID).val()+"']").html();
                        
                        if($text && $text!='')
                        {    
                            $text = htmlspecialchars_decode($text);
                        }
                        
                        $("#"+$ParentID+" input"+$ElementNo).val($text).blur();
                    }
                 else
                    {
                        $("#"+$ParentID+" input"+$ElementNo).val('').blur();
                    }
                
        }
        
        
        
        function htmlspecialchars_decode (string, quote_style) {
    // http://kevin.vanzonneveld.net
    // +   original by: Mirek Slugen
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Mateusz "loonquawl" Zalega
    // +      input by: ReverseSyntax
    // +      input by: Slawomir Kaniecki
    // +      input by: Scott Cariss
    // +      input by: Francois
    // +   bugfixed by: Onno Marsman
    // +    revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // +      input by: Ratheous
    // +      input by: Mailfaker (http://www.weedem.fr/)
    // +      reimplemented by: Brett Zamir (http://brett-zamir.me)
    // +    bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
    // *     returns 1: '<p>this -> &quot;</p>'
    // *     example 2: htmlspecialchars_decode("&amp;quot;");
    // *     returns 2: '&quot;'
    var optTemp = 0,
        i = 0,
        noquotes = false;
    if (typeof quote_style === 'undefined') {
        quote_style = 2;
    }
    string = string.toString().replace(/&lt;/g, '<').replace(/&gt;/g, '>');
    var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
    };
    if (quote_style === 0) {
        noquotes = true;
    }
    if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true;
            } else if (OPTS[quote_style[i]]) {
                optTemp = optTemp | OPTS[quote_style[i]];
            }
        }
        quote_style = optTemp;
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
        // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
    }
    if (!noquotes) {
        string = string.replace(/&quot;/g, '"');
    }
    // Put this in last place to avoid escape being double-decoded
    string = string.replace(/&amp;/g, '&');

    return string;
}
